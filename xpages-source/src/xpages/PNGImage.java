/*
XPAGES pour WebJaxe

Copyright (C) 2007 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.ImageIO; // JDK 1.4

public class PNGImage implements ImAnim {
    Image img = null;
    
    // debugging
    public static void main(String[] args) {
        if (args.length != 4) {
            System.err.println("il faut 4 arguments");
            return;
        }
        File forig = new File(args[0]);
        if (!forig.exists()) {
            System.err.println("le fichier n'existe pas");
            return;
        }
        File fdest = new File(args[1]);
        int largeur;
        int hauteur;
        try {
            largeur = (new Integer(args[2])).intValue();
            hauteur = (new Integer(args[3])).intValue();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
        PNGImage image = new PNGImage();
        try {
            image.open(forig);
            image.rescale(fdest, largeur, hauteur);
        } catch (IOException ex) {
            System.err.println("IOException : " + ex.getMessage());
            return;
        }
    }
    
    public void open(File f) throws IOException {
        //img = ImageIO.read(f);
        // bug: ALLBITS jamais envoy� avec drawImage
        // -> retour � la vieille m�thode  :-(
        
		img = Toolkit.getDefaultToolkit().getImage(f.getPath());
		
		try {
			while (img.getWidth(null) == -1 || img.getHeight(null) == -1)
				Thread.currentThread().sleep(50);
		} catch (InterruptedException ex) {
			throw new IOException("InterruptedException: " + ex.getMessage());
		}
    }
    
    public int getWidth() {
        if (img == null)
            return(-1);
        else
            return(img.getWidth(null));
    }
    
    public int getHeight() {
        if (img == null)
            return(-1);
        else
            return(img.getHeight(null));
    }
    
    public int getImageCount() {
        if (img == null)
            return(0);
        else
            return(1);
    }
    
    public Image getImage(int n) {
        if (n != 0)
            return(null);
        else
            return(img);
    }
    
    public void rescale(File newf, int width, int height) throws IOException {
        //System.out.println("rescale " + newf.getName() + " " + getWidth() + "x" + getHeight() + " -> " + width + "x" + height);
        
        //Image newimg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        BufferedImage bimg = getScaledBufferedImage(width, height);
        
        ImageIO.write(bimg, "PNG", newf);
    }
    
    public BufferedImage getScaledBufferedImage(int width, int height) {
        Image newimg =  Toolkit.getDefaultToolkit().createImage(
            new FilteredImageSource(img.getSource(), new AreaAveragingScaleFilter(width, height)));
        BufferedImage buffImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = buffImage.createGraphics();
        WaitingObserver wobs = new WaitingObserver();
        if (!g2.drawImage(newimg, 0, 0, wobs)) {
            wobs.attendre();
            g2.drawImage(newimg, 0, 0, null);
        }
        g2.dispose();

        return buffImage;
    }
    
    class WaitingObserver implements ImageObserver {
        boolean stillwaiting = true;
        public void attendre() {
            while (stillwaiting) {
                try {
                    Thread.currentThread().sleep(50);
                } catch (InterruptedException ex) {
                    System.err.println("InterruptedException: " + ex.getMessage());
                }
            }
        }
        public boolean imageUpdate(Image img, int infoflags, int x, int y, int width, int height) {
            if ((infoflags & ImageObserver.ALLBITS) != 0) {
                stillwaiting = false;
                return false;
            }
            if ((infoflags & ImageObserver.ERROR) != 0)
                System.err.println("imageUpdate: ERROR");
            if ((infoflags & ImageObserver.ABORT) != 0) {
                System.err.println("ABORT");
                stillwaiting = false;
                return false;
            }
            return true;
        }
    }
}
