/*
Jaxe - Editeur XML en Java

Copyright (C) 2006 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;

import javax.imageio.*;
import javax.imageio.metadata.*;
import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import jaxe.JaxeDocument;
import jaxe.JaxeResourceBundle;

import org.apache.log4j.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


// compilation : javac -source 1.5 -target 1.5 -encoding ISO-8859-1 -classpath .:Jaxe.jar pluginsjaxe/DialogueEquaTeX.java
/**
 * Dialogue pour JEEquaTeX.
 */
public class DialogueEquaTeX extends JDialog implements ActionListener, DocumentListener {

    private static final Logger LOG = Logger.getLogger(DialogueEquaTeX.class);
    private String serveur = null;
    private static String[] envPathMac = {"/bin","/usr/bin","/usr/local/bin","/usr/local/teTeX/bin/powerpc-apple-darwin-current","/sw/bin"};

    JFrame jframe;
    boolean valide = false;
    ImageIcon iconeq;
    JLabel labeleq;
    JTextArea zoneTexte;
    String texteEquation;
    JPanel epane;
    JaxeDocument doc;
    JTextField labelfield = null;
    String valeurLabel;

    public DialogueEquaTeX(JaxeDocument doc, String serveur, String texteEquation) {
        this(doc, serveur, texteEquation, null, null);
    }
    
    public DialogueEquaTeX(JaxeDocument doc, String serveur, String texteEquation,
            String nomlabel, String valeurLabel) {
        super(doc.jframe, JaxeResourceBundle.getRB().getString("equation.Equation"), true);
        this.doc = doc;
        this.serveur = serveur;
        jframe = doc.jframe;
        this.texteEquation = texteEquation;
        this.valeurLabel = valeurLabel;
        
        JPanel cpane = new JPanel(new BorderLayout());
        setContentPane(cpane);
        epane = new JPanel(new BorderLayout());
        iconeq = new ImageIcon();
        labeleq = new JLabel(iconeq);
        epane.add(labeleq, BorderLayout.CENTER);
        zoneTexte = new JTextArea(texteEquation, 2, 80);
        zoneTexte.setLineWrap(true);
        zoneTexte.setWrapStyleWord(true);
        zoneTexte.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        zoneTexte.getDocument().addDocumentListener(this);
        epane.add(zoneTexte, BorderLayout.SOUTH);
        cpane.add(epane, BorderLayout.CENTER);
        
        JPanel southpane = new JPanel(new GridLayout((nomlabel==null)?2:3, 1));
        
        JPanel apane = new JPanel(new FlowLayout(FlowLayout.CENTER));
        JButton boutonApercu = new JButton("Aper�u");
        boutonApercu.addActionListener(this);
        boutonApercu.setActionCommand("Apercu");
        apane.add(boutonApercu);
        southpane.add(apane);
        
        if (nomlabel != null) {
            JPanel lpane = new JPanel(new FlowLayout(FlowLayout.LEFT));
            lpane.add(new JLabel(nomlabel));
            labelfield = new JTextField(valeurLabel, 30);
            lpane.add(labelfield);
            southpane.add(lpane);
        }
        
        JPanel bpane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton boutonAnnuler = new JButton(JaxeResourceBundle.getRB().getString("bouton.Annuler"));
        boutonAnnuler.addActionListener(this);
        boutonAnnuler.setActionCommand("Annuler");
        bpane.add(boutonAnnuler);
        JButton boutonOK = new JButton(JaxeResourceBundle.getRB().getString("bouton.OK"));
        boutonOK.addActionListener(this);
        boutonOK.setActionCommand("OK");
        bpane.add(boutonOK);
        southpane.add(bpane);
        
        cpane.add(southpane, BorderLayout.SOUTH);
        
        getRootPane().setDefaultButton(boutonOK);
        setSize(new Dimension(500, 300));
        zoneTexte.requestFocus();
        addWindowListener(new WindowAdapter() {
            public void windowActivated(WindowEvent we) {
                javax.swing.SwingUtilities.invokeLater( new Runnable() {
                    public void run() {
                        zoneTexte.requestFocus();
                    }
                } );
            }
        });
        if (jframe != null) {
            Rectangle r = jframe.getBounds();
            setLocation(r.x + r.width/4, r.y + r.height/4);
        } else {
            Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screen.width - getSize().width)/3,(screen.height - getSize().height)/3);
        }
        
        if (texteEquation != null)
            majAffichage();
    }

    public boolean afficher() {
        setVisible(true);
        return(valide);
    }

    public String getTexte() {
        return(texteEquation);
    }
    
    public String getLabel() {
        return(valeurLabel);
    }
    
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        if ("OK".equals(cmd))
            actionOK();
        else if ("Annuler".equals(cmd)) {
            valide = false;
            setVisible(false);
        } else if ("Apercu".equals(cmd))
            majAffichage();
    }
    
    protected void actionOK() {
        valide = true;
        if (labelfield != null)
            valeurLabel = labelfield.getText();
        setVisible(false);
    }
    
    protected void changementTexte() {
        if (zoneTexte.getText().indexOf('\n') != -1)
            actionOK();
        else {
            texteEquation = zoneTexte.getText();
            //majAffichage();
        }
    }
    
    public void insertUpdate(DocumentEvent e) {
        changementTexte();
    }
    
    public void removeUpdate(DocumentEvent e) {
        changementTexte();
    }
    
    public void changedUpdate(DocumentEvent e) {
        changementTexte();
    }
    
    public static Image obtenirImageLocal(String texteEquation) throws IOException {
        String[] dangereux = {"include", "def", "command", "loop", "repeat", "open", "toks",
            "output", "input", "catcode", "name", "^^", "every", "errhelp", 
            "errorstopmode", "scrollmode", "nonstopmode", "batchmode", "read", 
            "write", "csname", "newhelp", "uppercase", "lowercase", "relax", 
            "aftergroup", "afterassignment", "expandafter", "noexpand",
            "special"};
        String textemod = texteEquation;
        for (int i=0; i<dangereux.length; i++)
            if (textemod.indexOf(dangereux[i]) != -1) {
                textemod = "\\textrm{\\color{red}Don't '" + dangereux[i] + "' me.}";
                break;
            }
        
        // sans tex2im, utilisation directe de latex, dvips et convert
        File tmpTEX = File.createTempFile("Jaxe_tmp_eq_", ".tex");
        tmpTEX.deleteOnExit();
        
        PrintWriter pw = new PrintWriter(new OutputStreamWriter(new FileOutputStream(tmpTEX), "ISO-8859-1"));
        pw.println("\\documentclass[12pt]{article}");
        pw.println("\\usepackage[latin1]{inputenc}");
        pw.println("\\usepackage{color}");
        pw.println("\\usepackage[dvips]{graphicx}");
        pw.println("\\IfFileExists{cmbright.sty}{\\usepackage{cmbright}}{\\message{Package cmbright.sty was not found.}}");
        pw.println("\\pagestyle{empty}");
        pw.println("\\pagecolor{white}");
        pw.println("\\begin{document}");
        pw.println("{\\color{black}");
        if (texteEquation.contains("eqnarray"))
            pw.println(textemod);
        else {
            pw.println("\\begin{eqnarray*}");
            
            pw.println("\\setbox1=\\hbox{$\\displaystyle " + textemod + "$}");
            pw.println("\\newdimen\\haut\n\\newdimen\\prof");
            pw.println("\\haut=\\ht1\n\\prof=\\dp1");
            pw.println("\\ifdim\\haut>\\prof\\prof=\\haut\\else\\haut=\\prof\\fi");
            pw.println("\\advance\\haut by .5em");
            pw.println("\\color{white}\\vrule height \\haut depth \\prof width 0.1pt\\color{black}\\box1");
            
            pw.println("\\end{eqnarray*}}");
        }
        pw.println("\\end{document}");
        pw.close();
        
        File parent = tmpTEX.getParentFile();
        exec("latex -interaction=batchmode " + tmpTEX.getAbsolutePath(), parent);
        String nomtmp = tmpTEX.getName();
        nomtmp = nomtmp.substring(0, nomtmp.indexOf('.'));
        (new File(parent, nomtmp + ".log")).delete();
        (new File(parent, nomtmp + ".aux")).delete();
        File tmpDVI = new File(parent, nomtmp + ".dvi");
        tmpDVI.deleteOnExit();
        File tmpEPS = new File(parent, nomtmp + ".eps");
        exec("dvips -o " + tmpEPS.getAbsolutePath() + " -E " + tmpDVI.getAbsolutePath(), null);
        tmpEPS.deleteOnExit();
        File tmpPNG = new File(parent, nomtmp + ".png");
        exec("convert +adjoin -antialias -density 90x90 " + tmpEPS.getAbsolutePath() + " " + tmpPNG.getAbsolutePath(), null);
        tmpPNG.deleteOnExit();
        return(Toolkit.getDefaultToolkit().createImage(tmpPNG.getAbsolutePath()));
    }
    
    private static String chercherChemin(String prog) {
        for (int i=0; i<envPathMac.length; i++)
            if (new File(envPathMac[i]+"/"+prog).exists())
                return(envPathMac[i]+"/"+prog);
        return(prog);
    }
    
    public static void exec(String cmd, File dir) throws IOException {
        String osName = System.getProperty("os.name");
        String[] envp = null;
        if (osName.startsWith("Mac")) {
            int indsp = cmd.indexOf(' ');
            String prog;
            if (indsp == -1)
                prog = cmd;
            else
                prog = cmd.substring(0, indsp);
            prog = chercherChemin(prog);
            if (indsp == -1)
                cmd = prog;
            else
                cmd = prog + cmd.substring(indsp);
            envp = new String[1];
            envp[0] = "PATH=" + envPathMac[0];
            for (int i=1; i<envPathMac.length; i++)
                envp[0] += ":" + envPathMac[i];
        }
        if (osName.indexOf("Windows") != -1)
            cmd = "cmd /c " + cmd;
        //System.out.println("exec \"" + cmd + "\"");
        //if (envp != null)
        //    System.out.println("envp[0]="+envp[0]);
        final Process process = Runtime.getRuntime().exec(cmd, envp, dir);
        final int r = doWaitFor(process);
        if (r != 0)
            throw new IOException("Erreur � l'ex�cution: " + r);
    }
    
    /**
     * Pour �viter que process.waitFor ne se bloque, il faut lire les sortie stdout et stderr.
     * Cette m�thode lit et ignore ces sorties, en attendant la fin du process.
     */
    private static int doWaitFor(Process p) {
        
        int exitValue = -1;  // returned to caller when p is finished
        
        try {
            
            final InputStream in  = p.getInputStream();
            final InputStream err = p.getErrorStream();
            
            boolean finished = false; // Set to true when p is finished
            
            while (!finished) {
                try {
                    while (in.available() > 0)
                        in.read();
                    
                    while (err.available() > 0)
                        err.read();
                    
                    // Ask the process for its exitValue. If the process
                    // is not finished, an IllegalThreadStateException
                    // is thrown. If it is finished, we fall through and
                    // the variable finished is set to true.
                    
                    exitValue = p.exitValue();
                    finished  = true;
                } catch (IllegalThreadStateException ex) {
                    // Process is not finished yet;
                    // Sleep a little to save on CPU cycles
                    Thread.currentThread().sleep(500);
                }
            }
        } catch (Exception e) {
            LOG.error("doWaitFor(): " + e.getClass().getName() + ": " + e.getMessage());
        }
        
        return exitValue;
    }
    
    public static Image obtenirImageServeur(String serveur, String texteEquation) {
        URL urlimage;
        if (serveur != null) {
            try {
                urlimage = new URL(serveur + "?" + URLEncoder.encode(texteEquation, "UTF-8"));
            } catch (MalformedURLException ex) {
                LOG.error("obtenirImageServeur: MalformedURLException: " + ex.getMessage(), ex);
                return(null);
            } catch (UnsupportedEncodingException ex) {
                LOG.error("obtenirImageServeur: UnsupportedEncodingException: " + ex.getMessage(), ex);
                return(null);
            }
        } else {
            LOG.error("obtenirImageServeur: serveur non sp�cifi� dans le fichier de config");
            return(null);
        }
        /*
        Avec une r�f�rence vers l'applet, on pourrait utiliser le serveur o� il y a l'applet comme serveur d'�quations,
        mais il n'existe pas encore de serveur d'�quations multi-plateforme int�gr� � WebJaxe, et il n'est pas facile
        d'obtenir une r�f�rence vers l'applet.
        else {
            JApplet applet = ?;
            try {
                urlimage = new URL(applet.getDocumentBase(), serveurApplet + "?" + URLEncoder.encode(texteEquation, "UTF-8"));
            } catch (MalformedURLException ex) {
                LOG.error("obtenirImageServeur: MalformedURLException: " + ex.getMessage());
                return(null);
            } catch (UnsupportedEncodingException ex) {
                LOG.error("obtenirImageServeur: UnsupportedEncodingException: " + ex.getMessage());
                return(null);
            }
        }
        */
        return(Toolkit.getDefaultToolkit().createImage(urlimage));
    }
    
    public static Image obtenirImage(String serveur, String texteEquation, boolean applet) {
        Image img;
        if (!applet) { // c'est l'application Jaxe
            try {
                img = obtenirImageLocal(texteEquation);
            } catch (IOException ex) {
                LOG.error("latex en local: IOException: " + ex.getMessage());
                if (serveur != null) {
                    LOG.error("essai avec le serveur...");
                    img = obtenirImageServeur(serveur, texteEquation);
                } else {
                    LOG.error("l'adresse du serveur n'a pas �t� sp�cifi�e dans le fichier de config, on ne peut pas faire d'essai par le serveur");
                    img = null;
                }
            }
        } else { // c'est une applet
            if (serveur != null) {
                img = obtenirImageServeur(serveur, texteEquation);
            } else {
                LOG.error("l'adresse du serveur n'a pas �t� sp�cifi�e dans le fichier de config, on ne peut pas obtenir l'image de l'�quation sur le serveur");
                img = null;
            }
        }
        return(img);
    }
    
    protected void majAffichage() {
        Image img;
        if (texteEquation == null || "".equals(texteEquation))
            img = null;
        else
            img = obtenirImage(serveur, remplacerCommandes(texteEquation, doc), !doc.jframe.getClass().getName().equals("jaxe.JaxeFrame"));
        if (img == null)
            img = new BufferedImage(10, 10, BufferedImage.TYPE_INT_ARGB);
        iconeq.setImage(img);
        labeleq.repaint();
    }
    
    // position de la commande (par ex. \abc) dans le texte
    // par exemple posCommande("\a", "\abc \a") renvoit 5
    // -1 si pas trouv�
    private static int posCommande(String cmd, String texte) {
        int ind = texte.indexOf(cmd);
        char c = ' ';
        if (ind != -1 && ind + cmd.length() < texte.length())
            c = texte.charAt(ind + cmd.length());
        while (ind != -1 && ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))) {
            int ind2 = texte.substring(ind+1).indexOf(cmd);
            if (ind2 == -1)
                ind = -1;
            else
                ind = ind + 1 + ind2;
            c = ' ';
            if (ind != -1 && ind + cmd.length() < texte.length())
                c = texte.charAt(ind + cmd.length());
        }
        return(ind);
    }
    
    public static String remplacerCommandes(String texteEquation, JaxeDocument doc) {
        NodeList liste = doc.DOMdoc.getElementsByTagName("DEFCMDTEX");
        for (int i=0; i<liste.getLength(); i++) {
            Element def = (Element)liste.item(i);
            String commande = def.getAttribute("commande");
            if (commande.charAt(0) != '\\')
                commande = "\\" + commande;
            String snb = def.getAttribute("nb-params");
            int nb = 0;
            try {
                nb = Integer.parseInt(snb);
            } catch (NumberFormatException ex) {
            }
            String remplacement = def.getAttribute("remplacement");
            int ind = posCommande(commande, texteEquation);
            int posArg;
            int np;
            while (ind != -1) {
                posArg = ind + commande.length();
                np = nb;
                String tmpremp = remplacement;
                while (np > 0) {
                    int indp = tmpremp.indexOf("#" + (nb - np + 1));
                    if (indp != -1) {
                        if (texteEquation.charAt(posArg) == '{') {
                            int finpos = posArg + 1;
                            int profondeur = 0;
                            while (finpos < texteEquation.length() && (profondeur > 0 || texteEquation.charAt(finpos) != '}')) {
                                if (texteEquation.charAt(finpos) == '{')
                                    profondeur++;
                                else if (texteEquation.charAt(finpos) == '}')
                                    profondeur--;
                                finpos++;
                            }
                            //System.out.print(tmpremp);
                            if (finpos < texteEquation.length())
                                tmpremp = tmpremp.substring(0, indp) + texteEquation.substring(posArg+1, finpos) +
                                    tmpremp.substring(indp+2);
                            //System.out.println(" -> " + tmpremp);
                            posArg = finpos + 1;
                        }
                    }
                    np --;
                }
                texteEquation = texteEquation.substring(0, ind) + tmpremp + texteEquation.substring(posArg);
                //System.out.println("-->   " + texteEquation);
                ind = posCommande(commande, texteEquation);
            }
        }
        return(texteEquation);
    }
    
    /**
     * Cr�ation de l'image correspondant au texte de l'�quation
     */
    public static BufferedImage creerImage(Component comp, String serveur, String texteEquation, JaxeDocument doc) {
        // reconstruction de l'image
        Image img1 = obtenirImage(serveur, remplacerCommandes(texteEquation, doc), !doc.jframe.getClass().getName().equals("jaxe.JaxeFrame"));
        if (img1 == null)
            return(null);
        MediaTracker tracker = new MediaTracker(comp);
        tracker.addImage(img1, 0);
        try {
            tracker.waitForAll();
        } catch (InterruptedException ex) {
        }
        int w = img1.getWidth(null);
        int h = img1.getHeight(null);
        if (w == -1 || h == -1) {
            LOG.error("Erreur � l'obtention des dimensions de l'image ");
            JOptionPane.showMessageDialog(doc.jframe, "Erreur � l'obtention des dimensions de l'image ", JaxeResourceBundle.getRB().getString("erreur.Erreur"), JOptionPane.ERROR_MESSAGE);
            return(null);
        }
        Dimension dim = new Dimension(w, h);
        
        // r�cup�ration dans une BufferedImage
        BufferedImage img2 = new BufferedImage(dim.width, dim.height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = img2.createGraphics();
        g2.setColor(Color.white);
        g2.fillRect(0, 0, dim.width, dim.height);
        g2.setColor(Color.black);
        g2.drawImage(img1, 0, 0, null);
        
        // application masque
        // IE ne g�re pas les PNG avec une palette -> utilisation d'une �chelle de gris avec transparence
        ColorSpace grayColorSpace = ColorSpace.getInstance(ColorSpace.CS_GRAY);
        final ColorModel cm = new ComponentColorModel(grayColorSpace, true, true, Transparency.TRANSLUCENT, DataBuffer.TYPE_BYTE);
        final WritableRaster raster = cm.createCompatibleWritableRaster(dim.width, dim.height);
        final BufferedImage img3 = new BufferedImage(cm, raster, false, null);
        int[] rgbArray = new int[dim.width*dim.height];
        img2.getRGB(0, 0, dim.width, dim.height, rgbArray, 0, dim.width);
        for (int i=0; i<rgbArray.length; i++)
            rgbArray[i] = (255 - (rgbArray[i] & 0x000000FF)) << 24;
        img3.setRGB(0, 0, dim.width, dim.height, rgbArray, 0, dim.width);
        
        return(img3);
    }
    
    /**
     * Enregistrement de l'image de l'�quation vers une sortie donn�e en param�tre.
     * Le param�tre sortie peut �tre de la classe File (pour enregistrer dans un fichier) ou OutputStream.
     * Les images peuvent �tre utilis�es dans IE6 (PNG avec une �chelle de gris et fond blanc, couche alpha
     * utilisable par les autres navigateurs).
     */
    public static void enregistrerImage(final BufferedImage img, final Object sortie) throws IOException {
        // IE met un fond gris si aucun bKGD n'est sp�cifi� -> ajout de bKGD dans les m�tadonn�es
        ImageWriter premierPourPNG = ImageIO.getImageWritersByFormatName("PNG").next();
        IIOMetadata metadata = premierPourPNG.getDefaultImageMetadata(new ImageTypeSpecifier(img), null);
        Element racine = (Element)metadata.getAsTree("javax_imageio_png_1.0");
        NodeList nl = racine.getElementsByTagName("bKGD");
        if (nl.getLength() == 0) {
            Node suivant = racine.getFirstChild();
            String name = suivant.getNodeName();
            if (("IHDR".equals(name) || "PLTE".equals(name)))
                suivant = suivant.getNextSibling();
            if (suivant != null && "PLTE".equals(suivant.getNodeName()))
                suivant = suivant.getNextSibling();
            Element bKGD = new IIOMetadataNode("bKGD");
            if (suivant == null)
                racine.appendChild(bKGD);
            else
                racine.insertBefore(bKGD, suivant);
            Element bKGD_Grayscale = new IIOMetadataNode("bKGD_Grayscale");
            bKGD.appendChild(bKGD_Grayscale);
            bKGD_Grayscale.setAttribute("gray", "255");
            try {
                metadata.mergeTree("javax_imageio_png_1.0", racine);
            } catch (IIOInvalidTreeException ex) {
                LOG.error("enregistrerImage(BufferedImage, Object)", ex);
            }
        }
        // enregistrement (peut lancer une IOException)
        premierPourPNG.setOutput(ImageIO.createImageOutputStream(sortie));
        premierPourPNG.write(new IIOImage(img, null, metadata));
    }
}
