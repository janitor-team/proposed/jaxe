/*
Jaxe - Editeur XML en Java

Copyright (C) 2010 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages.jeimage;

import jaxe.JaxeResourceBundle;
import jaxe.JaxeDocument;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;

import org.w3c.dom.DOMException;
import org.w3c.dom.Element;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.text.DefaultEditorKit;

import java.net.MalformedURLException;
import java.net.URL;
import java.net.URISyntaxException;

import java.util.ArrayList;

import java.io.File;

import org.w3c.dom.*;
import javax.xml.parsers.*;

import java.security.AccessControlException;


/**
 * Affiche un dialogue permettant de choisir une image parmis les fichiers de la contribution
 */
public class DialogueImage extends JDialog implements ActionListener {
    /**
     * Logger for this class
     */
    private static final Logger LOG = Logger.getLogger(DialogueImage.class);
    
    private static final int windowHeight = 460;
    private static final int leftWidth = 300;
    private static final int rightWidth = 340;
    private static final int windowWidth = leftWidth + rightWidth;
    
    boolean valide = false;
    Element refElement;
    Element el;
    JFrame jframe;
    JaxeDocument doc;
    String srcAttr;
    DomJTree djtree;
    JLabel label = null;
    Document mydoc;
    
    /**
     * construit un JTree � partir d'un adaptateur de model
     * @param jframe 
     * @param doc Document Jaxe
     * @param titre Titre de la fen�tre
     * @param refElement L'�l�ment de r�ference
     * @param el L'�l�ment
     * @param srcAttr L'attribut nom
     * @param deplier Si true, on d�plie le JTree
     */
    public DialogueImage(final JFrame jframe, final JaxeDocument doc, final String titre, final Element refElement, final Element el, final String srcAttr, boolean deplier) {
        super(jframe, titre, true);
        this.jframe = jframe;
        this.doc = doc;
        this.refElement = refElement;
        this.el = el;
        this.srcAttr = srcAttr;
        
        String cheminIMG = el.getAttribute(srcAttr);
        
        // cas d'une application
        if (doc.furl == null || doc.fsave !=null) {
            File cheminContrib = doc.fsave.getParentFile();
            XmlDomAppli xmldomappli = new XmlDomAppli(cheminContrib);
            mydoc = xmldomappli.getDocXMLappli();
        }
        
        // cas d'une applet
        else {
            XmlDom xmldom = new XmlDom(construireURLphp());
            mydoc = xmldom.getDocXML();
        }
        
        
        
        final JPanel cpane = new JPanel(new BorderLayout());
        setContentPane(cpane);
        
        if (label == null) {
            label = new JLabel();
            label.setVerticalAlignment(javax.swing.SwingConstants.TOP);
            cpane.add(label);
        }
        
         
        // construction de DOM et l'afficher dans le JTree
        djtree = new DomJTree(doc, mydoc,cheminIMG, deplier, label);
        
        // ajout du JTree au panel
        cpane.add(djtree, BorderLayout.CENTER);
        
        final JPanel bpane = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        final JButton boutonAnnuler = new JButton(JaxeResourceBundle.getRB().getString("bouton.Annuler"));
        boutonAnnuler.addActionListener(this);
        boutonAnnuler.setActionCommand("Annuler");
        bpane.add(boutonAnnuler);
        final JButton boutonOK = new JButton(JaxeResourceBundle.getRB().getString("bouton.OK"));
        boutonOK.addActionListener(this);
        boutonOK.setActionCommand("OK");
        bpane.add(boutonOK);
        
        // JTree, vue de gauche
        JScrollPane vueTree = new JScrollPane(djtree.tree);
        vueTree.setPreferredSize(new Dimension( leftWidth, windowHeight ));
        
        // apercu des images, vue de droite
        JScrollPane apercu = new JScrollPane(label);
        
        // construction d'une vue splitPane
        JSplitPane splitPane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT,vueTree,apercu );
        splitPane.setContinuousLayout( true );
        splitPane.setDividerLocation( leftWidth );
        splitPane.setPreferredSize(new Dimension( windowWidth + 10, windowHeight+10 ));
        
        cpane.setLayout(new BorderLayout());
        cpane.add("Center", splitPane );
        cpane.add(bpane, BorderLayout.SOUTH);
        getRootPane().setDefaultButton(boutonOK);
        cpane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        
        pack();
        
        if (jframe != null) {
            final Rectangle r = jframe.getBounds();
            setLocation(r.x + r.width/4, r.y + r.height/4);
        } else {
            final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((screen.width - getSize().width)/3, (screen.height - getSize().height)/3);
        }
    } //constructeur DialogueImage


    /**
     * construit l'URL du fichier PHP
     * @return une URL sous forme de String
     */
    public String construireURLphp() {
        String cheminFichierXML = doc.furl.getFile();
        String[] fictmp = cheminFichierXML.split("\\" + "/");
        String nomFichierXML = fictmp[fictmp.length-1];
        String[] chtmp = nomFichierXML.split("\\.xml");
        String Contrib = chtmp[0];
        
        //invoque la m�thode getURLEnregistrement() de la classe JaxeAppletFrame
        URL urlEnr = null;
        try {
                Class<?> c = Class.forName("jaxeapplet.JaxeAppletFrame");
                Class[] types = new Class[] {}; 
                try {
                    Method methode = c.getMethod("getURLEnregistrement", types);
                    Object resultat = methode.invoke(doc.jframe, (Object[])null);
                    urlEnr = (URL)resultat;
                } catch (final NoSuchMethodException ex) {
                    LOG.error("Erreur Method", ex); 
                }
            } catch (final Exception ex) {
                LOG.error("Erreur Class", ex); 
            }
            
        URL urlPHP;
        String cheminPHP = "";
        try {
            urlPHP = new URL(urlEnr,"liste_fichiers_contrib.php");
            cheminPHP = urlPHP.toString();
        } catch (MalformedURLException ex) {
            LOG.error("construireURLphp()", ex); 
        }
    
        String adresse = cheminPHP+"?contrib="+Contrib;
        return adresse;
    } //construireURLphp
    
    
    
    public boolean afficher() {
        setVisible(true);
        return valide;
    }


    /**
     * enregistre le chemin de l'image dans l'attribut srcAttr
     */
    public void enregistrerReponses() {
        String cheminImage = djtree.cheminNoeud(djtree.arbreSelection);
        final ArrayList<Element> attributs = doc.cfg.listeAttributs(refElement);
        try {
            for (final Element attdef : attributs) {
                if (srcAttr.equals(doc.cfg.nomAttribut(attdef)))
                    el.setAttributeNS(doc.cfg.espaceAttribut(attdef), srcAttr, cheminImage);
            }
            doc.setModif(true);
        } catch (final DOMException ex) {
            LOG.error("enregistrerReponses()", ex);
            return;
        }
    }
    

    
    public void actionPerformed(final ActionEvent e) {
        final String cmd = e.getActionCommand();
        if ("OK".equals(cmd)) {
            if (djtree.arbreSelection != null && djtree.adpNoeud.estUnDossier()) {
                Toolkit.getDefaultToolkit().beep();
                valide = false;
                setVisible(true);
            }
            else {
                valide = true;
                setVisible(false);
            }
        } else if ("Annuler".equals(cmd)) {
            valide = false;
            setVisible(false);
        } 
    }
    
} //fin class DialogueImage
