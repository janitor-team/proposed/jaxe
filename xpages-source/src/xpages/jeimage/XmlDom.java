/*
Jaxe - Editeur XML en Java

Copyright (C) 2010 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages.jeimage;

import org.apache.log4j.Logger;

import javax.xml.parsers.*;

import org.w3c.dom.*;

import org.xml.sax.InputSource;


/**
 * Cr�e un document DOM � partir d'un fichier xml
 */
public class XmlDom {
    /**
     * Logger for this class
     */
    private static final Logger LOG = Logger.getLogger(XmlDom.class);
    
    private static final String[] extension = new String []{"jpg","jpeg","png","gif","ogv","ogg","mpg","mpeg"};
    
    private DocumentBuilderFactory factory;
    private DocumentBuilder builder;
    private Element arborescence;
    
    private Document docXML;
    
    /**
     * construit un nouveau objet DOM Document � partir d'un fichier xml
     * @param adresse l'URL du fichier PHP (en forme de String) qui g�n�re le xml
     */
    public XmlDom(final String adresse) {
        try {
            factory = DocumentBuilderFactory.newInstance();
            builder = factory.newDocumentBuilder();
            docXML = builder.parse(new InputSource(adresse));
            arborescence = docXML.getDocumentElement();
            filtrerDOM();
        } catch (Exception ex) {
            LOG.error("XmlDom(String)", ex);
        }
    }
    
    
    /**
     * retourne le Document XML
     */
    public Document getDocXML() {
        return docXML;
    }


    /**
     * filtre le DOM pour afficher que des fichiers images
     */
    private void filtrerDOM() {
        final NodeList parcoursF = arborescence.getElementsByTagName("FICHIER");
        final Element[] tabEl = new Element[parcoursF.getLength()];

        for (int i=0; i<parcoursF.getLength(); i++) {
            tabEl[i] = (Element)parcoursF.item(i);
        }

        for (Element elF: tabEl) {
            if ( !typeImage(elF) ) {
                try { 
                    elF.getParentNode().removeChild(elF);
                } catch (DOMException ex) { 
                    LOG.error("filtrerDOM()", ex);
                }
            }
        }
    }
    
    /**
     * renvoie true si le type de fichier est une image
     */
    private boolean typeImage(Element eF) {
        for (String ext: extension) {
            if ( eF.getAttribute("nom").endsWith("."+ext) || eF.getAttribute("nom").endsWith("."+ext.toUpperCase()) )
                return true;
        }
        return false;
    }
    
} //fin class XmlDom

