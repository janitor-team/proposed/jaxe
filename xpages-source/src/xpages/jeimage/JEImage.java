/*
Jaxe - Editeur XML en Java

Copyright (C) 2010 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages.jeimage;

import jaxe.elements.JEFichier;
import jaxe.JaxeDocument;
import jaxe.JaxeResourceBundle;

import org.apache.log4j.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import java.io.File;



/**
 * Fichier d'image. L'image est affich�e dans le texte si elle est trouv�e, sinon un message d'erreur
 * est affich� dans le texte � la place de l'image.
 * Type d'�l�ment Jaxe: 'fichier'
 * param�tre: srcAtt: le nom de l'attribut donnant le nom du fichier
 */
public class JEImage extends JEFichier {
    /**
     * Logger for this class
     */
    private static final Logger LOG = Logger.getLogger(JEImage.class);

    public JEImage(final JaxeDocument doc) {
        super(doc);
    }
    
    
    @Override
    public Node nouvelElement(final Element refElement) {
        final String nombalise = doc.cfg.nomElement(refElement);
        final Element newel = nouvelElementDOM(doc, refElement);
        if (newel == null) return null;

        final String srcAttr = doc.cfg.valeurParametreElement(refElement, "srcAtt", defaultSrcAttr);
        
        if (doc.furl != null || doc.fsave != null) {
            final DialogueImage dlg = new DialogueImage(doc.jframe, doc,
                JaxeResourceBundle.getRB().getString("zone.NouvelleBalise") + " " + nombalise, refElement, newel, srcAttr, false);
            if (!dlg.afficher())
                return null;
            try {
                dlg.enregistrerReponses();
            } catch (final Exception ex) {
                LOG.error("nouvelElement(Element)", ex);
                return null;
            }
        }
        else {
            JOptionPane.showMessageDialog(doc.jframe, JaxeResourceBundle.getRB().getString("source.SauverAvant"),
            JaxeResourceBundle.getRB().getString("erreur.Erreur"), JOptionPane.ERROR_MESSAGE);
            return null;
        }
        
        if (doc.fsave != null) {
            final File f = new File(doc.fsave.getParent() + File.separatorChar + newel.getAttribute(srcAttr));
            if (!f.exists()) {
                JOptionPane.showMessageDialog(doc.jframe,
                JaxeResourceBundle.getRB().getString("erreur.FichierNonTrouve"),
                JaxeResourceBundle.getRB().getString("zone.NouvelleBalise") + " " + nombalise,
                JOptionPane.ERROR_MESSAGE);
            }
        }
        return newel;
    }
    
    
    @Override
    public void afficherDialogue(final JFrame jframe) {
        final Element el = (Element)noeud;
        final DialogueImage dlg = new DialogueImage(doc.jframe, doc,"Fichier", refElement, el, srcAttr, true);
        if (!dlg.afficher())
            return;
        dlg.enregistrerReponses();
        majAffichage();
    }
}
