/*
XPAGES pour WebJaxe

Copyright (C) 2009 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package xpages;

import java.awt.*;
import java.io.*;

/**
 * Seules les m�thodes getWidth et getHeight sont impl�ment�es pour OggAnim,
 * il n'y a pas de d�codage de la vid�o.
 */
public class OggAnim implements ImAnim {
	
    int largeur;
    int hauteur;
    
	// debugging
	public static void main(String[] args) {
		if (args.length != 1) {
			System.err.println("il faut 1 argument");
			return;
		}
		File forig = new File(args[0]);
		if (!forig.exists()) {
			System.err.println("le fichier n'existe pas");
			return;
		}
		OggAnim anim = new OggAnim();
		try {
			anim.open(forig);
			System.out.println("largeur: " + anim.getWidth());
			System.out.println("hauteur: " + anim.getHeight());
		} catch (IOException ex) {
			System.err.println("IOException : " + ex.getMessage());
			return;
		}
	}
	
	public void open(File f) throws IOException {
        FileInputStream in = new FileInputStream(f);
        long signature = 0x4F676753; // OggS
        int b1,b2,b3,b4;
        int bi = 0;
        b1 = in.read();
        b2 = in.read();
        b3 = in.read();
        b4 = in.read();
        long v = ((b1 & 0xFF) << 24) | ((b2 & 0xFF) << 16) | ((b3 & 0xFF) << 8) | (b4 & 0xFF);
        if (v != signature) {
            in.close();
            throw new IOException("Erreur: Le fichier " + f.getPath() + " n'a pas la signature du format Ogg");
        }
        signature = 0x80746865; // identification header
        v = ((b1 & 0xFF) << 24) | ((b2 & 0xFF) << 16) | ((b3 & 0xFF) << 8) | (b4 & 0xFF);
        while (bi != -1 && v != signature) {
            bi = in.read();
            if (bi != -1) {
                b1 = b2; b2 = b3; b3 = b4;
                b4 = bi;
                v = ((b1 & 0xFF) << 24) | ((b2 & 0xFF) << 16) | ((b3 & 0xFF) << 8) | (b4 & 0xFF);
            }
        }
        if (v == signature) {
            for (int i=0; i<10; i++)
                b1 = in.read(); // on saute 10 octets apr�s 'the'
            b1 = in.read();
            b2 = in.read();
            b3 = in.read();
            largeur = ((b1 & 0xFF) << 16) | ((b2 & 0xFF) << 8) | (b3 & 0xFF); // PICW
            b1 = in.read();
            b2 = in.read();
            b3 = in.read();
            hauteur = ((b1 & 0xFF) << 16) | ((b2 & 0xFF) << 8) | (b3 & 0xFF); // PICH
        } else
            System.err.println("Erreur: Dimensions non trouv�es pour " + f.getPath());
        in.close();
	}
	
	public int getWidth() {
		return(largeur);
	}
	
	public int getHeight() {
		return(hauteur);
	}
	
	public int getImageCount() {
        System.err.println("Erreur: getImageCount n'est pas impl�ment� pour les vid�o OGG");
        return(0);
	}
	
	public Image getImage(int n) {
        System.err.println("Erreur: getImage n'est pas impl�ment� pour les vid�o OGG");
        return(null);
	}
	
	public void rescale(File newf, int width, int height) throws IOException {
        throw new IOException("rescale n'est pas impl�ment� pour les vid�o OGG");
	}
    
}
