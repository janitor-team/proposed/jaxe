/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import org.w3c.dom.*;
import javax.swing.text.BadLocationException;


/**
* Classe qui affiche dans un JPanel les champs nec�ssaires pour les Menus, les Menus d'insertion et les Menus de fonction
*/
public class EditeurDeMenusB extends JPanel {
    
    private ListeElements listeElements;
	private FenetreEdition fe;
    private EditeurDeMenus edm;
    private Document doc;
    private Element racine;
    private Element menus;
    
    private static final String[] type_noeud_dom = {"element", "instruction", "commentaire", "cdata"};
    private static final String[] type_noeud = {Strings.get("label.element"), Strings.get("label.instruction"), Strings.get("label.commentaire"), Strings.get("label.cdata")};
    
    private InfosNoeud monInfosNoeud;
    private Node monNoeud;
    private DefaultMutableTreeNode monNoeudSelectionne;
    private JPanel panelAffichage;
    private JPanel panMenu;
    private JPanel panelMenuInsertion;
    private JPanel panelMenuFonction;
    
    private DefaultTreeModel m_model;
    private JTree m_tree;
    
    private JTextField nomMenu_t;
    private JTextField nomMenuIns_t;
    private JComboBox typeNoeud_c;
    private JTextField raccourciM_t;
    private JTextField nomFonction_t;
    private JTextField nomClasse_t;
    private JTextField raccourciF_t;
    
    private JButton bEditerTxtM;
    private JButton bEditerTxtMi;
    private JButton boutonMoins;
    
    private JPanel listePanelParametre;
    
    
    
    public EditeurDeMenusB(final ListeElements listeElements, final DefaultTreeModel model_h, final JTree tree_h, final FenetreEdition fe, final EditeurDeMenus edm, final Document doc, final Element racine) {
        this.listeElements = listeElements;
        m_model = model_h;
        m_tree = tree_h;
        this.fe = fe;
        this.edm = edm;
        this.doc = doc;
        this.racine = racine;
        menus = Outils.premierEnfantDeNom(racine, "MENUS");
        
        afficher();
    }
    
    
    private void afficher() {
        
        // ecouteur jtree
        m_tree.addTreeSelectionListener(new TreeSelectionListener() {
            public void valueChanged(TreeSelectionEvent e) {
                Object selectionCourante = m_tree.getLastSelectedPathComponent(); 
                if (selectionCourante != null) {
                    monNoeudSelectionne = (DefaultMutableTreeNode) selectionCourante;
                    if (monNoeudSelectionne.getUserObject() instanceof InfosNoeud) {
                        monInfosNoeud = (InfosNoeud)monNoeudSelectionne.getUserObject();
                        monNoeud = monInfosNoeud.getNoeud();
                        if (!monNoeudSelectionne.isRoot()) {
                            // si la s�lection est un menu
                            if ("MENU".equals(monNoeud.getNodeName())) {
                                bEditerTxtM.setEnabled(true);
                                panMenu.setVisible(true);
                                panelMenuInsertion.setVisible(false);
                                panelMenuFonction.setVisible(false);
                                if (monNoeud != null) {
                                    nomMenu_t.setText(((Element)monNoeud).getAttribute("nom"));
                                    nomMenu_t.requestFocus();
                                    nomMenu_t.selectAll();
                                }
                            }
                            // si la s�lection est un menu d'insertion
                            if ("MENU_INSERTION".equals(monNoeud.getNodeName())) {
                                panMenu.setVisible(false);
                                bEditerTxtMi.setEnabled(true);
                                panelMenuInsertion.setVisible(true);
                                panelMenuFonction.setVisible(false);
                                
                                nomMenuIns_t.setText(((Element)monNoeud).getAttribute("nom"));
                                if (! "".equals(((Element)monNoeud).getAttribute("type_noeud")))
                                    typeNoeud_c.setSelectedItem(Strings.get("label."+((Element)monNoeud).getAttribute("type_noeud")));
                                raccourciM_t.setText(((Element)monNoeud).getAttribute("raccourci"));
                            }
                            
                            // si la s�lection est un menu fonction
                            if ("MENU_FONCTION".equals(monNoeud.getNodeName())) {
                                panMenu.setVisible(false);
                                nomFonction_t.setText(selectionCourante.toString());
                                
                                bEditerTxtMi.setEnabled(false);
                                panelMenuInsertion.setVisible(false);
                                panelMenuFonction.setVisible(true);
                                
                                final Element menu_fonction = (Element)monNoeud;
                                if (menu_fonction != null) {
                                    nomFonction_t.requestFocus();
                                    nomFonction_t.selectAll();
                                    nomFonction_t.setText(menu_fonction.getAttribute("nom"));
                                    nomClasse_t.setText(menu_fonction.getAttribute("classe"));
                                    raccourciF_t.setText(menu_fonction.getAttribute("raccourci"));
                                }
                                recupererParametre(menu_fonction);
                            }
                        }
                        // si root
                        else {
                            nomMenu_t.setText("");
                            panMenu.setVisible(false);
                            panelMenuInsertion.setVisible(false);
                            panelMenuFonction.setVisible(false);
                        }
                    }
                }
                // si pas de s�l�ction
                else { 
                    bEditerTxtM.setEnabled(false);
                    bEditerTxtMi.setEnabled(false);
                    panMenu.setVisible(false);
                    panelMenuInsertion.setVisible(false);
                    panelMenuFonction.setVisible(false);
                }
            }
        });// fin ecouteur jtree
        
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(700,400));
        
        creerPanelBoutons();     
        
        /***********************  Panel Affichage *******************/
        panelAffichage = new JPanel();
        panelAffichage.setLayout(new BoxLayout(panelAffichage, BoxLayout.Y_AXIS));
        //panelAffichage.setBorder(new TitledBorder(Strings.get("label.Menu")));
        final JScrollPane defilementAffichage = new JScrollPane(panelAffichage);
        
        /** Menu **/
        panMenu = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelAffichage.add(panMenu);
        panMenu.setVisible(false);
        final JLabel nomMenu_l = new JLabel(Strings.get("label.Nom"));
        nomMenu_t = new JTextField(15);
        panMenu.add(nomMenu_l);
        panMenu.add(nomMenu_t);
        // ecouteur sur le nom de menu
        nomMenu_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurNomMenu(de);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurNomMenu(de);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurNomMenu(de);
            }
        });
        
        panMenu.add(bEditerTxtM);
        
        /** panel Menu insertion **/
        creerPanelMenuInsertion();
        
        /** panel Menu fonction **/
        creerPanelMenuFonction();
        
        add(defilementAffichage,BorderLayout.CENTER);
        
    }//fin afficher
    
    
    /**
    * Cr�e le panel Boutons
    */
    private void creerPanelBoutons() {
        JPanel panelBoutonsBas = new JPanel(new FlowLayout());
        add(panelBoutonsBas,BorderLayout.SOUTH);
        
        final JButton bTester = new JButton(new AbstractAction("Tester") {
            public void actionPerformed(ActionEvent e) {
                EditeurFichierConfig.convertToString(doc);
            }
        });
        //panelBoutonsBas.add(bTester);
        
        final JButton bFermer = new JButton(new AbstractAction(Strings.get("bouton.Fermer")) {
            public void actionPerformed(ActionEvent e) {
                nettoyerDOM(menus);
                edm.setVisible(false);
                fe.afficher();
            }
        });
        panelBoutonsBas.add(bFermer);
        
        bEditerTxtM = new JButton(new AbstractAction(Strings.get("bouton.EditerTextes")) {
            public void actionPerformed(ActionEvent e) {
                editerTextesM();
            }
        });
        bEditerTxtM.setEnabled(false);
        
        bEditerTxtMi = new JButton(new AbstractAction(Strings.get("bouton.EditerTextes")) {
            public void actionPerformed(ActionEvent e) {
                editerTextesMi();
            }
        });
        bEditerTxtMi.setEnabled(false);
    }
    
    
    /**
    * Cr�e le panel Menu Insertion
    */
    private void creerPanelMenuInsertion() {
        
        panelMenuInsertion = new JPanel(new GridLayout(3,2));
        panelAffichage.add(panelMenuInsertion);
        panelMenuInsertion.setVisible(false);
        panelMenuInsertion.setBorder(new TitledBorder(Strings.get("label.MenuIns")));
        
        final JPanel panNomMenuInsertion = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelMenuInsertion.add(panNomMenuInsertion);
        final JLabel nomMenuIns = new JLabel(Strings.get("label.Nom"));
        nomMenuIns_t = new JTextField(15);
        nomMenuIns_t.setEditable(false);
        
        panNomMenuInsertion.add(nomMenuIns);
        panNomMenuInsertion.add(nomMenuIns_t);
        panNomMenuInsertion.add(bEditerTxtMi);
        
        final JPanel panTypeNoeud = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelMenuInsertion.add(panTypeNoeud);
        final JLabel typeNoeud = new JLabel(Strings.get("label.TypeNoeud"));
        typeNoeud_c = new JComboBox(type_noeud);
        typeNoeud_c.setSelectedIndex(0);
        
        //�couteur sur le champs type de noeud
        typeNoeud_c.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent ie) {
                if (typeNoeud_c.getSelectedItem() != null) {
                    ((Element)monNoeud).setAttribute("type_noeud", type_noeud_dom[typeNoeud_c.getSelectedIndex()]);
                    EditeurFichierConfig.setModif(true);
                }
            }
        });
            
        panTypeNoeud.add(typeNoeud);
        panTypeNoeud.add(typeNoeud_c);
        
        final JPanel panRaccourciM = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelMenuInsertion.add(panRaccourciM);
        final JLabel raccourciM = new JLabel(Strings.get("label.raccourci"));
        raccourciM_t = new JTextField(15);
        //�couteur sur le champs raccourci
        raccourciM_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurRaccourciM(de);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurRaccourciM(de);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurRaccourciM(de);
            }
        });
    
        
        panRaccourciM.add(raccourciM);
        panRaccourciM.add(raccourciM_t);
    }
    
    
    /**
    * Cr�e le panel Menu Fonction
    */
    private void creerPanelMenuFonction() {
        
        panelMenuFonction = new JPanel();
        panelMenuFonction.removeAll();
        panelMenuFonction.setLayout(new BoxLayout(panelMenuFonction, BoxLayout.Y_AXIS));
        panelAffichage.add(panelMenuFonction);
        panelMenuFonction.setBorder(new TitledBorder(Strings.get("label.MenuFonction")));
        panelMenuFonction.setVisible(false);
        
        final JPanel panNomFonction = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelMenuFonction.add(panNomFonction);
        final JLabel nomFonction = new JLabel(Strings.get("label.Nom"));
        nomFonction_t = new JTextField(15);
        /*if (menu_fonction != null)
            nomFonction_t.setText(menu_fonction.getAttribute("nom"));*/
        panNomFonction.add(nomFonction);
        panNomFonction.add(nomFonction_t);
        // ecouteur sur le champ nom
        nomFonction_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurNomFonction(de);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurNomFonction(de);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurNomFonction(de);
            }
        });
        
        final JPanel panNomClasse = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelMenuFonction.add(panNomClasse);
        final JLabel nomClasse = new JLabel(Strings.get("label.NomClasse"));
        nomClasse_t = new JTextField(15);
        /*if (menu_fonction != null)
            nomClasse_t.setText(menu_fonction.getAttribute("classe"));*/
        
        panNomClasse.add(nomClasse);
        panNomClasse.add(nomClasse_t);
        // ecouteur sur le champ classe
        nomClasse_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurClasse(de);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurClasse(de);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurClasse(de);
            }
        });
        
        final JPanel panRaccourciF = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelMenuFonction.add(panRaccourciF);
        final JLabel raccourciF = new JLabel(Strings.get("label.raccourci"));
        raccourciF_t = new JTextField(15);
        /*if (menu_fonction != null)
            raccourciF_t.setText(menu_fonction.getAttribute("raccourci"));*/
        
        panRaccourciF.add(raccourciF);
        panRaccourciF.add(raccourciF_t);
        // ecouteur sur le champ raccourci
        raccourciF_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurRaccourciF(de);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurRaccourciF(de);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurRaccourciF(de);
            }
        });
        
        // panel Param�tre
        listePanelParametre = new JPanel();
        listePanelParametre.setLayout(new BoxLayout(listePanelParametre, BoxLayout.Y_AXIS));
        panelMenuFonction.add(listePanelParametre);
    }
    
    
    /**
    * Cr�e le panel Parametre
    */
    private void creerPanelParametre(final Element parametre, final int index) {
        final JPanel panParametreTout = new JPanel(new FlowLayout(FlowLayout.LEFT));
        listePanelParametre.add(panParametreTout);
        
        final JPanel panParametre = new JPanel(new GridLayout(2,1));
        panParametreTout.add(panParametre);
        panParametre.setBorder(new TitledBorder(Strings.get("label.Parametre")));
        
        final JPanel panNomParametre = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panParametre.add(panNomParametre);
        final JLabel nomParametre = new JLabel(Strings.get("label.Nom"));
        final JTextField nomParametre_t = new JTextField(15);
        if (parametre != null)
            nomParametre_t.setText(parametre.getAttribute("nom"));
        panNomParametre.add(nomParametre);
        panNomParametre.add(nomParametre_t);
        // �couteur sur nom parametre
        nomParametre_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "nom");
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "nom");
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "nom");
            }
        });
        
        final JPanel panValeurParametre = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panParametre.add(panValeurParametre);
        final JLabel valeurParametre = new JLabel(Strings.get("label.valeurParametre"));
        final JTextField valeurParametre_t = new JTextField(15);
        if (parametre != null)
            valeurParametre_t.setText(parametre.getAttribute("valeur"));
        panValeurParametre.add(valeurParametre);
        panValeurParametre.add(valeurParametre_t);
        // �couteur sur valeur parametre
        valeurParametre_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "valeur");
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "valeur");
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurParametre(de, index, "valeur");
            }
        });
        
        final JPanel panPlusMoins = new JPanel(new FlowLayout());
        panParametreTout.add(panPlusMoins);
        
        final JButton boutonPlus = new JButton(new AbstractAction("+") {
            public void actionPerformed(ActionEvent e) {
                if (!("".equals(nomParametre_t.getText()))) {
                    if (!("".equals(valeurParametre_t.getText()))) {
                        //boutonMoins.setEnabled(false);
                        creerPanelParametre(null, listePanelParametre.getComponentCount());
                        listePanelParametre.revalidate();
                    }
                }
            }
        });
        panPlusMoins.add(boutonPlus);
        
        boutonMoins = new JButton(new AbstractAction("-") {
            public void actionPerformed(ActionEvent e) {
                if(listePanelParametre.getComponentCount() > 1) {
                    listePanelParametre.remove(panParametreTout);
                    listePanelParametre.revalidate();
                    Element menu_fonction = (Element)monNoeud;
                    Element element_parametre = trouverParametre(menu_fonction, index);
                    if (element_parametre != null)
                        element_parametre.getParentNode().removeChild(element_parametre);
                }
            }
        });
        panPlusMoins.add(boutonMoins);
    }
    
    
    
    /**
    * Edite les textes d'un menu
    * @param Le document doc
    * @param Le menu s�lectionn� sous forme de String
    */
    private void editerTextesM() {
        new TextesMenu(doc, monNoeud);
    }
    
    /**
    * Edite les textes d'un menu insertion
    * @param Le document doc
    * @param Le menu insertion s�lectionn� sous forme de String
    */
    private void editerTextesMi() {
        new TextesMenuInsertion(doc, monNoeud);
    }
    
    
    
    
    /**
    * �couteur sur le champ nom de Menu
    * @param Un DocumentEvent
    */
    private void ecouteurNomMenu(final DocumentEvent de) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteMenu = source.getText(0, source.getLength());
            Element menu = (Element)monNoeud;
            if ("MENU".equals(menu.getNodeName()))
                menu.setAttribute("nom", texteMenu);
            
            monInfosNoeud.majTitre();
            m_model.reload(monNoeudSelectionne);
            m_tree.repaint();
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    
    /**
    * Ecouteur sur le champ raccourci du menu insertion
    * @param Un DocumentEvent
    */
    private void ecouteurRaccourciM(final DocumentEvent de) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteR = source.getText(0, source.getLength());
            Element menu_insertion = (Element)monNoeud;
            menu_insertion.setAttribute("raccourci", texteR);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    
    
    /**
    * Ecouteur sur le champ nom du menu fonction
    * @param Un DocumentEvent
    */
    private void ecouteurNomFonction(final DocumentEvent de) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteN = source.getText(0, source.getLength());
            Element menu_fonction = (Element)monNoeud;
            menu_fonction.setAttribute("nom", texteN);
            
            monInfosNoeud.majTitre();
            m_model.reload(monNoeudSelectionne);
            m_tree.repaint();
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    /**
    * Ecouteur sur le champ classe du menu fonction
    * @param Un DocumentEvent
    */
    private void ecouteurClasse(final DocumentEvent de) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteC = source.getText(0, source.getLength());
            Element menu_fonction = (Element)monNoeud;
            menu_fonction.setAttribute("classe", texteC);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    /**
    * Ecouteur sur le champ raccourci du menu fonction
    * @param Un DocumentEvent
    */
    private void ecouteurRaccourciF(final DocumentEvent de) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteR = source.getText(0, source.getLength());
            Element menu_fonction = (Element)monNoeud;
            menu_fonction.setAttribute("raccourci", texteR);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    
    /**
    * Ecouteur sur le champ Parametre
    * @param Un DocumentEvent
    * @param L'index
    * @param le champ d�sign�
    */
    private void ecouteurParametre(final DocumentEvent de, final int index, final String champP) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteP = source.getText(0, source.getLength());
            Element menu_fonction = (Element)monNoeud;
            if ("nom".equals(champP))
                enregistrerParametre(menu_fonction, index, texteP, null);
            if ("valeur".equals(champP))
                enregistrerParametre(menu_fonction, index, null, texteP);
                
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }


    /**
    * �couteur sur le mod�le d'arbre
    */
	/*private void ecouteurArbreModele(final Element menu) {
        m_model.addTreeModelListener(new TreeModelListener() {
            public void treeNodesChanged(TreeModelEvent tme) {
                ecouteurAM(tme, menu);
            }

            public void treeNodesInserted(TreeModelEvent tme) {
            }

            public void treeNodesRemoved(TreeModelEvent tme) {
            }

            public void treeStructureChanged(TreeModelEvent tme) {
            }
        });
    }*/
    
    /**
    * Modifie le titre d'un noeud dans le JTree et le DOM
    */
    /*private void ecouteurAM(final TreeModelEvent tme, final Element menu) {
        TreePath tp = tme.getTreePath();
        Object[] enfants = tme.getChildren();
        DefaultMutableTreeNode noeudChange;
        
        if (enfants != null)
            noeudChange = (DefaultMutableTreeNode) enfants[0];
        else
            noeudChange = (DefaultMutableTreeNode) tp.getLastPathComponent();

        menu.setAttribute("nom", noeudChange.getUserObject().toString());
    }*/
    
    
    /*********************************** Op�ration DOM **********************************************/
    // PARAMETRE
    private Element enregistrerParametre(final Element menu_fonction, final int index, final String nomP, final String valeurP) {
        Element parametre = trouverParametre(menu_fonction, index);
        if (parametre == null) {
            parametre = doc.createElement("PARAMETRE");
            menu_fonction.appendChild(parametre);
        }
        if (nomP != null)
            parametre.setAttribute("nom", nomP);
        if (valeurP != null)
            parametre.setAttribute("valeur", valeurP);
        
        return parametre;
    }
    
    private Element trouverParametre(final Element menu_fonction, int index) {
        if (index == -1)
            return(null);
        if (menu_fonction == null)
            return(null);
        int ip = 0;
        for (Node n = menu_fonction.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "PARAMETRE".equals(n.getNodeName())) {
                if (ip == index)
                    return((Element)n);
                ip++;
            }
        }
        return(null);
    }
    
    
    
    /**
    * Remplie le(s) champ(s) Parametre � partir du DOM
    */
    private void recupererParametre(final Element menu_fonction) {
        listePanelParametre.removeAll();
    
        if (menu_fonction != null) {
            Element parametre = Outils.premierEnfantDeNom(menu_fonction, "PARAMETRE");
            if (parametre == null)
                parametre = enregistrerParametre(menu_fonction, 0, null, null);
                
            creerPanelParametre(parametre, 0);
        
            int enfants = menu_fonction.getChildNodes().getLength();
            for (int i = 1; i < enfants ; i++) {
                parametre = Outils.enfantSuivantDeMemeNom(parametre);
                if (parametre != null)
                    creerPanelParametre(parametre, i);
            }
        }
        else
            creerPanelParametre(null, 0);
    }
    
    
    private void nettoyerDOM(final Node nodeParent) {
        for (Node n = nodeParent.getFirstChild(); n != null; n = n.getNextSibling())
            if (n instanceof Element && "MENU".equals(n.getNodeName())) {
                nettoyerDOM(n);
                for (Node nd = n.getFirstChild(); nd != null; nd = nd.getNextSibling())
                    if (nd instanceof Element && "MENU_FONCTION".equals(nd.getNodeName()))
                        for (Node ndd = nd.getFirstChild(); ndd != null; ndd = ndd.getNextSibling())
                            if ( "PARAMETRE".equals(ndd.getNodeName()) && (!((Element)ndd).hasAttribute("nom") || !((Element)ndd).hasAttribute("valeur") || "".equals(((Element)ndd).getAttribute("nom")) || "".equals(((Element)ndd).getAttribute("valeur"))) )
                                nd.removeChild(ndd);
            }
    }
    
}
