/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import org.w3c.dom.*;
import java.io.*;
import java.util.*;


public class ChoixRacine extends JDialog {
    
    
    private Object parentSelectionne = null;
    private Object racineSelectionne = null;
    
    private Document doc;
    private Element racine;
    private Element langage;
    
    private ListeElements listeElements;
    private JList listeP;
    private JList listeR;
    private JList listeRestante = new JList();
    private DefaultListModel listeModelParents;
    private DefaultListModel listeModeleRestant;
    private DefaultListModel listeModelRacines;
    
    private ArrayList<String> elementsRestants = new ArrayList<String>();
    
    private JButton boutonAjouter;
    private JButton boutonRetirer;
    
    public ChoixRacine(final Document doc, final Element racine, final ListeElements listeElements) {
        super(new JFrame(), Strings.get("titre.ChoixRacines"));
        this.doc = doc;
        this.racine = racine;
        this.listeElements = listeElements;
        langage = Outils.premierEnfantDeNom(racine, "LANGAGE");
        
        listeElements.JListeElementsParents();
        listeModelParents = listeElements.getListModelP();
        
        listeR = new JList();
        listeR.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);      
        
        retirerRacinesExistantes();
        if (elementsRestants != null) {
            listeP = listeRestante;
            listeModelParents = listeModeleRestant;
        }
        else 
            listeP = listeElements.JListeDesElements();
        
        listeP.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
    }


    public void afficher() {
        creerPanelListes();
        
        listeP.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) { 
                parentSelectionne = listeP.getSelectedValue();
                if (parentSelectionne != null) { 
                    boutonAjouter.setEnabled(true);
                    boutonRetirer.setEnabled(false);
                }
                else {
                    boutonAjouter.setEnabled(false);
                    boutonRetirer.setEnabled(false);
                }
            }
        });
        
        listeR.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) { 
                racineSelectionne = listeR.getSelectedValue();
                if (racineSelectionne != null) { 
                    boutonAjouter.setEnabled(false);
                    boutonRetirer.setEnabled(true);
                }
                else {
                    boutonAjouter.setEnabled(false);
                    boutonRetirer.setEnabled(false);
                }
            }
        });
        
        creerPanelBoutonsBas();
        
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
    
    
    /**
    * Cr�e les panels de listes
    */
    private void creerPanelListes() {
        setLayout(new BorderLayout());
        JPanel panelHaut = new JPanel(new FlowLayout());
        add(panelHaut,BorderLayout.NORTH);
        
         // panel liste des �l�ments parents
        JPanel panelLP = new JPanel(new BorderLayout());
        panelHaut.add(panelLP);
        JLabel labelP = new JLabel(Strings.get("label.ElementsParents"));
        panelLP.add(labelP, BorderLayout.NORTH);
        final JScrollPane defilementListeP = new JScrollPane(listeP);
        defilementListeP.setPreferredSize(new Dimension(200, 300));
        panelLP.add(defilementListeP, BorderLayout.CENTER);
        
        // boutons Ajouter Retirer
        final JPanel panelB = new JPanel();
        panelB.setLayout(new BoxLayout(panelB, BoxLayout.Y_AXIS));
        panelHaut.add(panelB);
        
        
        boutonAjouter = new JButton(new AbstractAction(Strings.get("bouton.Ajouter")+" ->") {
            public void actionPerformed(ActionEvent e) {
                ajouter();
            }
        });
        boutonAjouter.setEnabled(false);
        panelB.add(boutonAjouter); 
        
        panelB.add(Box.createVerticalStrut(10));
        
        boutonRetirer = new JButton(new AbstractAction("<- "+Strings.get("bouton.Retirer")) {
            public void actionPerformed(ActionEvent e) {
                retirer();
            }
        });
        boutonRetirer.setEnabled(false);
        panelB.add(boutonRetirer);
    
        // panel liste des �l�ments racine
        JPanel panelLR = new JPanel(new BorderLayout());
        panelHaut.add(panelLR);
        JLabel labelR = new JLabel(Strings.get("label.Racines"));
        panelLR.add(labelR, BorderLayout.NORTH);
        final JScrollPane defilementListeR = new JScrollPane(listeR);
        defilementListeR.setPreferredSize(new Dimension(200, 300));
        panelLR.add(defilementListeR, BorderLayout.CENTER);
    }
    
    
    /**
    * Cr�e le bouton fermer
    */
    private void creerPanelBoutonsBas() {
        JPanel panelBoutons = new JPanel();
        add(panelBoutons, BorderLayout.SOUTH);
        
        final JButton bFermer = new JButton(new AbstractAction(Strings.get("bouton.Fermer")) {
            public void actionPerformed(ActionEvent e) {
                if (listeModelRacines.getSize() == 0)
                    JOptionPane.showMessageDialog(ChoixRacine.this, Strings.get("message.AuMoinsUneRacine"), Strings.get("titre.Message"), JOptionPane.ERROR_MESSAGE);
                else
                    setVisible(false);
            }
        });
        panelBoutons.add(bFermer);
    }
    
    
    /**
    * Retire les �l�ments racines existants dans le DOM 
    */
    private void retirerRacinesExistantes() {
        
        listeModeleRestant = new DefaultListModel();
        Object[] eltsListe = listeModelParents.toArray();
        
        // remplir le nouveau modele
        for (int j = 0; j < eltsListe.length; j++) {
            listeModeleRestant.addElement(eltsListe[j]);
        }
        
        Object[] eltsR = listeModeleRestant.toArray();
        for (int j = 0; j < eltsR.length; j++)
            elementsRestants.add(eltsR[j].toString());
        
        listeRestante.setModel(listeModeleRestant);
        
        listeModelRacines = new DefaultListModel();
        for (Node n = langage.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "RACINE".equals(n.getNodeName())) {
                String nomRacine = ((Element)n).getAttribute("element");
                for (int i = 0; i < listeModeleRestant.getSize(); i++) {
                    if (nomRacine.equals(listeModeleRestant.get(i).toString())) {
                        listeModeleRestant.removeElement(listeModeleRestant.get(i));
                        listeModelRacines.addElement(nomRacine);
                    }
                }
            }
        }
        
        trierListe(listeModelRacines);
        listeR.setModel(listeModelRacines);
    }
    
    
    /**
    * Trie la liste des nom des �l�ments par nom croissant
    */
    private void trierListe(final DefaultListModel maListeModele) {
        Object[] contenu = maListeModele.toArray();  
        Arrays.sort(contenu);
        maListeModele.removeAllElements();
        for (int i = 0; i< contenu.length;i++)
            maListeModele.addElement(contenu[i]);
    }
    
    
    
    /**
    * Ajouter un �l�ment parent � la liste des racines
    */
    private void ajouter() {
        if (parentSelectionne != null) {
            listeModelRacines.addElement(parentSelectionne);
            enregistrerElementRacine(parentSelectionne);
            listeModelParents.removeElement(parentSelectionne);
            
            trierListe(listeModelRacines);
        }
    }
    
    /**
    * Retire un �l�ment racine
    */
    private void retirer() {
        if (racineSelectionne != null) {
            listeModelParents.addElement(racineSelectionne);
            
            Element elementRacine = Outils.getElementSelectionne(langage, "RACINE", racineSelectionne.toString(), "element");
            if (elementRacine != null)
                langage.removeChild(elementRacine);
            
            listeModelRacines.removeElement(racineSelectionne);
            
            trierListe(listeModelParents);
        }
    }
    
    
    /**
    * Enregistre l'�l�ment RACINE dans le DOM
    */
    private void enregistrerElementRacine(final Object racine) {
        Element elementRacine = Outils.getElementSelectionne(langage, "RACINE", racine.toString(), "element");
        if (elementRacine == null) {
            elementRacine = doc.createElement("RACINE");
            elementRacine.setAttribute("element", racine.toString());
            langage.appendChild(elementRacine);
            
            EditeurFichierConfig.setModif(true);
        }
    }
}
