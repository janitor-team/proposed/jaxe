/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import jaxe.InterfaceSchema;
import jaxe.wxs.JaxeWXS;
import jaxe.SchemaRelaxNG;
import jaxe.SchemaSimple;
import jaxe.Config;
import jaxe.JaxeException;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import java.net.URL;
import org.w3c.dom.*;


/**
* Repr�sente la liste des �l�ments
*/
public class ListeElements {
    
    private InterfaceSchema schema;
    
    private ArrayList<Element> tabEl = new ArrayList<Element>();
    
    private JTextField jTexteRecherche;	
    private ArrayList<String> lesElements = new ArrayList<String>();
    private ArrayList<String> lesElementsParents = new ArrayList<String>();
    private ArrayList<Element> listeElementsParents = new ArrayList<Element>();
    private JList listeDesElements;
    private DefaultListModel listModel;
    private DefaultListModel nouveauModele;
    
    private JList listeDesElementsParents;
    private DefaultListModel modelParents;
    
    private ArrayList<String> listeAttributs = new ArrayList<String>();
    
    /**
    * R�cup�re les �l�ments d'un sch�ma � partir de son url
    */
    public ListeElements(final URL schemaURL, final Config cfg, final Element schema_simple) {
        
        if (schemaURL != null && schema_simple == null) {
            if (schemaURL.toString().endsWith("xsd") || schemaURL.toString().endsWith("XSD")) {
                try {
                    schema = new JaxeWXS(schemaURL, cfg);
                } catch (final JaxeException jex) {
                    System.out.println("JaxeWXS: "+jex);
                } 
            }
            
            if (schemaURL.toString().endsWith("rng") || schemaURL.toString().endsWith("RNG")) {
                schema = new SchemaRelaxNG(schemaURL, cfg);
            }
        }
        
        if (schema_simple != null && schemaURL == null)
            schema = new SchemaSimple(schema_simple, cfg);
        
        if (schema != null) {
            tabEl = schema.listeTousElements();
            recupererLesElements();
        }
    }
    
    
    /**
    * R�cup�re la liste des noms des �l�ments du sch�ma, et les trie par nom croissant
    */
    public void recupererLesElements() {
        lesElements.clear();   
        
        for (int i = 0; i< tabEl.size();i++) {
            String nomEl = schema.nomElement(tabEl.get(i));
            if (!lesElements.contains(nomEl))
                lesElements.add(nomEl);
        }
        
        Collections.sort(lesElements);
    }
    
    /**
    * Renvoie la liste des noms des �l�ments
    * @return La liste des noms des �l�ments
    */
    public ArrayList<String> getLesElements() {
        return lesElements;
    }
    
    
    /**
    * Retourne la liste des attributs d'un �l�ment du sch�ma
    * @param La r�ference de l'�l�ment
    * @return La liste des attributs de l'�l�ment el
    */
    public ArrayList<String> getAttributElement(final Element el) {
        listeAttributs.clear();
        if (schema != null) {
            ArrayList<Element> lAtt = schema.listeAttributs(el);
            if (lAtt != null && lAtt.size() > 0) {
                for (int i=0; i<lAtt.size();i++)
                    listeAttributs.add(schema.nomAttribut(lAtt.get(i)));
                return listeAttributs;
            }
        }
        return null;
    }
    
    
    /**
    * Retourne la r�ference d'un �l�ment dans le sch�ma � partir du nom
    * @param Le nom de l'�l�ment sous forme de String
    * @return La r�ference de l'�l�ment
    */
    public Element getReferenceElement(String s) {
        if (schema != null)
            return schema.referenceElement(s);
        return null;
    }
    
    
    /**
    * Retourne la liste des �l�ments parent du sch�ma class�s par ordre alphab�tique
    */
    public void getElementsParents() {
        for (int i = 0; i< tabEl.size();i++) {
            listeElementsParents = schema.listeElementsParents(tabEl.get(i));
            for (int j = 0; j< listeElementsParents.size();j++) {
                if (!lesElementsParents.contains(schema.nomElement(listeElementsParents.get(j))))
                    lesElementsParents.add(schema.nomElement(listeElementsParents.get(j)));
            }
        }
        
        Collections.sort(lesElementsParents);
    }
    
    /**
    * Retourne la liste des �l�ments parent du sch�ma
    * @return La liste des �l�ments parent dans une JList
    */
    public JList JListeElementsParents() {
        
        getElementsParents();
        if (listeDesElementsParents == null) {
            listeDesElementsParents = new JList();
            listeDesElementsParents.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            modelParents = new DefaultListModel();
            for (int i = 0; i< lesElementsParents.size();i++)
                modelParents.addElement(lesElementsParents.get(i));
        }
        listeDesElementsParents.setModel(modelParents);
            
		return listeDesElementsParents;
    }
    
    
    /**
    * Retourne la liste de tous les �l�ments du sch�ma
    * @return La liste de tous les �l�ments dans une JList
    */
    public JList JListeDesElements() {
		if (listeDesElements == null) {
			listeDesElements = new JList();
            listeDesElements.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			listModel = new DefaultListModel();
			for (int i = 0; i< lesElements.size();i++)
                listModel.addElement(lesElements.get(i));
			listeDesElements.setModel(listModel);
		}
		return listeDesElements;
    }
    
    
    /**
    * Retourne un champ texte qui permet de filtrer une JList
    * @return Un champ JTextField
    */
    public JTextField getJTexteRecherche(/*final ArrayList<String> MesElements*/) {
        
        //final ArrayList<String> elementsListe = (MesElements != null) ? MesElements : lesElements;
        
        if (jTexteRecherche == null) {
            jTexteRecherche = new JTextField(15);
            jTexteRecherche.setMaximumSize(jTexteRecherche.getPreferredSize());
			//Ajout d'un listener lors d'un relachement d'une touche de clavier
			jTexteRecherche.addKeyListener(new KeyAdapter() {
				public void keyReleased(KeyEvent e) {
                    nouveauModele = new DefaultListModel();
					String texteSaisi = jTexteRecherche.getText();					
					for (int i = 0; i < lesElements.size(); i++) {
						//Comparaison des elements contenu dans l ArrayList et du texte saisi 
						if (lesElements.get(i).toString().indexOf(texteSaisi) != -1) {
							nouveauModele.addElement(lesElements.get(i));
						}
					}
					listeDesElements.setModel(nouveauModele);//On definie ce nouveau model pour la JList
				}
			});				
		}
		return jTexteRecherche;
	}
    
    /**
    * Retourne le mod�le de la liste
    * @return Le mod�le de la JList
    */
    public DefaultListModel getListModel() {
        return listModel;
    }
    
     /**
    * Retourne le mod�le de la liste des �l�ments Parents
    * @return Le mod�le de la JList
    */
    public DefaultListModel getListModelP() {
        return modelParents;
    }
}
