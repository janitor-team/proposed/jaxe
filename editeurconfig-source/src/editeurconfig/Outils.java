/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import org.w3c.dom.*;

public class Outils {
    
    public static Element premierEnfantDeNom(final Element parent, final String nomEnfant) {
        Node n = parent.getFirstChild();
        while (n != null) {
            if (n instanceof Element) {
                Element el = (Element)n;
                if (el.getTagName().equals(nomEnfant))
                    return(el);
            }
            n = n.getNextSibling();
        }
        return(null);
    }
    
    public static Element enfantSuivantDeMemeNom(final Element el) {
        if (el == null)
            return(null);
        Node n = el;
        while (n.getNextSibling() != null) {
            n = n.getNextSibling();
            if (n instanceof Element && n.getNodeName().equals(el.getNodeName()))
                return((Element)n);
        }
        return(null);
    }
    
    public static String getValeurElement(final Element el) {
        Node n = el.getFirstChild();
        if (n instanceof Text)
            return(n.getNodeValue());
        return(null);
    }
    
    public static void setValeurElement(final Document doc, final Element el, final String valeur) {
        Node n = el.getFirstChild();
        if (n == null)
            el.appendChild(doc.createTextNode(valeur));
        else
            n.setNodeValue(valeur);
    }
    
    
    
    
    public static Element getElementSelectionne(Element parent, String nom, String selectionne, String attr) {
        for (Node n = parent.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && nom.equals(n.getNodeName())) {
                if (((Element)n).getAttribute(attr).equals(selectionne))
                    return((Element)n);
            }
        }
        return null;
    }
    
}
