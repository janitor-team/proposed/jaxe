/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import org.w3c.dom.*;
import javax.swing.text.BadLocationException;


/**
* Affiche dans un jframe l'�diteur des textes pour un affichage attribut
*/
public class TextesAttribut extends JFrame {
    
    private Document doc;
    private Element racine;
    private JPanel panelAffichage;
    private Node monNoeud;
    private String attributSelectionne;
    
    public TextesAttribut(final Document doc, final Node monNoeud, final String attributSelectionne) {
        super(Strings.get("titre.TextePourUnAttribut"));
        
        this.doc = doc;
        this.monNoeud = monNoeud;
        this.attributSelectionne = attributSelectionne;
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
    }
    
    
    private void afficher() {
        final JPanel panelFenetre = new JPanel(new BorderLayout());
        panelFenetre.add(new JLabel(Strings.get("label.TextePourLAttribut")+" "+attributSelectionne),BorderLayout.NORTH);
        
        panelAffichage = new JPanel();
        panelAffichage.setLayout(new BoxLayout(panelAffichage, BoxLayout.Y_AXIS));
        final JScrollPane defilement = new JScrollPane(panelAffichage);
        defilement.setPreferredSize(new Dimension(400, 700));
        panelFenetre.add(defilement,BorderLayout.CENTER);
        
        creerPanelLangues();
        
        final JPanel panelBoutons = new JPanel(new FlowLayout());
        
        final JButton bFermer = new JButton(new AbstractAction(Strings.get("bouton.Fermer")) {
            public void actionPerformed(ActionEvent e) {
                nettoyerDOM();
                setVisible(false);
            }
        });
        panelBoutons.add(bFermer);
        
        final JButton bTester = new JButton(new AbstractAction("Tester") {
            public void actionPerformed(ActionEvent e) {
                EditeurFichierConfig.convertToString(doc);
            }
        });
        //panelBoutons.add(bTester);
        
        panelFenetre.add(panelBoutons,BorderLayout.SOUTH);
        add(panelFenetre);
        
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
       
       
    /**
    * Affiche le panel Textes Attribut
    * @param L'�l�ment strings_element
    * @param L'�l�ment affichage_attribut
    */
    private void afficherTextesAttribut(final Element strings_element, final Element affichage_attribut, final JPanel panel) {
        final Element strings_attribut = enregistrerStringsAttribut(strings_element, affichage_attribut.getAttribute("attribut"));
        
        afficherTitreDocumentation(strings_attribut, panel);
        
        for (Node n = affichage_attribut.getFirstChild(); n != null; n = n.getNextSibling()) {
            if (n instanceof Element && "VALEUR_SUGGEREE".equals(n.getNodeName())) {
                afficherTitreValeur(strings_attribut, (Element)n, panel);
            }
        }
    }
        
    
    /**
    * Affiche le panel Titre Documentation
    * @param L'�l�ment parent
    * @param panel destination
    */
    private void afficherTitreDocumentation(final Element element_strings_attribut, final JPanel panel) {
        final JPanel panTitre = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(panTitre);
        JLabel titre_l = new JLabel(Strings.get("label.Titre")+": ");
        JTextField titre_t = new JTextField(15);
        panTitre.add(titre_l);
        panTitre.add(titre_t);
        
        Element titre = Outils.premierEnfantDeNom(element_strings_attribut, "TITRE");
        if (titre != null)
            titre_t.setText(Outils.getValeurElement(titre));
            
        // �couteur sur titre
        titre_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurTitre(de, element_strings_attribut);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurTitre(de, element_strings_attribut);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurTitre(de, element_strings_attribut);
            }
        });
        
        final JPanel panDocumentation = new JPanel(new GridLayout(2,1));
        panel.add(panDocumentation);
        JLabel documentation_l = new JLabel(Strings.get("label.Documentation")+": ");
        JTextArea documentation_t = new JTextArea(4, 12);
        documentation_t.setLineWrap(true);
        documentation_t.setWrapStyleWord(true);
        JScrollPane scrollDoc = new JScrollPane(documentation_t);
        panDocumentation.add(documentation_l);
        panDocumentation.add(scrollDoc);
        
        Element documentation = Outils.premierEnfantDeNom(element_strings_attribut, "DOCUMENTATION");
        if (documentation != null)
            documentation_t.setText(Outils.getValeurElement(documentation));
            
        // �couteur sur documentation
        documentation_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurDocumentation(de, element_strings_attribut);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurDocumentation(de, element_strings_attribut);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurDocumentation(de, element_strings_attribut);
            }
        });
    }
    
    
    
    /**
    * Affiche le panel Titre Valeur
    * @param L'�l�ment parent
    * @param L'�l�ment valeur_suggerre
    * @param panel destination
    */
    private void afficherTitreValeur(final Element element_strings_attribut, final Element valeur_suggerre, final JPanel panel) {
        final JPanel panTitreValeur = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(panTitreValeur);
        final String valeur = Outils.getValeurElement(valeur_suggerre);
        JLabel titreValeur_l = new JLabel(Strings.get("label.TitreValeur")+" '"+valeur+"'");
        JTextField titreValeur_t = new JTextField(15);
        panTitreValeur.add(titreValeur_l);
        panTitreValeur.add(titreValeur_t);
        
        Element titre_valeur = Outils.getElementSelectionne(element_strings_attribut, "TITRE_VALEUR", valeur, "valeur");
        if (titre_valeur != null)
            titreValeur_t.setText(Outils.getValeurElement(titre_valeur));
            
        // �couteur sur titre
        titreValeur_t.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate(DocumentEvent de) {
                ecouteurTitreValeur(de, element_strings_attribut, valeur);
            }
            public void insertUpdate(DocumentEvent de) {
                ecouteurTitreValeur(de, element_strings_attribut, valeur);
            }
            public void removeUpdate(DocumentEvent de) {
                ecouteurTitreValeur(de, element_strings_attribut, valeur);
            }
        });
    }
    
    
    
    /**
    * Panel de langues
    * Pour chaque langue on �dite les textes de l'attribut
    */
    private void creerPanelLangues() {
        
        racine = doc.getDocumentElement();
        NodeList listeNoeud = racine.getElementsByTagName("STRINGS");
        int nombreEnf = listeNoeud.getLength();
        
        final JPanel panelLangue[] = new JPanel[nombreEnf];
        
        for (int i=0; i<nombreEnf; i++) {
            if (listeNoeud.item(i) instanceof Element) {
                Element strings = (Element)listeNoeud.item(i);
                final Element strings_element = enregistrerStringsElement(strings);
                panelLangue[i] = new JPanel();
                
                panelLangue[i].setLayout(new BoxLayout(panelLangue[i], BoxLayout.Y_AXIS));
                panelAffichage.add(panelLangue[i]);
                panelLangue[i].setBorder(new TitledBorder(strings.getAttribute("langue").toUpperCase()));
                
                final Element affichage_noeuds = Outils.premierEnfantDeNom(racine, "AFFICHAGE_NOEUDS");
                final Element affichage_element = (Element)monNoeud;
                Element affichage_attribut = Outils.getElementSelectionne(affichage_element, "AFFICHAGE_ATTRIBUT", attributSelectionne, "attribut");
                if (affichage_attribut != null)
                    afficherTextesAttribut(strings_element, affichage_attribut, panelLangue[i]);
            }
        }
    }
    
    
    /*************************************** Ecouteurs **********************************************/
    /**
    * Ecouteur sur le champ Titre
    * @param Un DocumentEvent
    * @param L'�l�ment parent
    */
    private void ecouteurTitre(final DocumentEvent de, final Element strings_attribut) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteTitre = source.getText(0, source.getLength());
            enregistrerTitre(strings_attribut, texteTitre);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    /**
    * Ecouteur sur le champ Documentation
    * @param Un DocumentEvent
    * @param L'�l�ment parent
    */
    private void ecouteurDocumentation(final DocumentEvent de, final Element strings_attribut) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteDocumentation = source.getText(0, source.getLength());
            enregistrerDocumentation(strings_attribut, texteDocumentation);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
        }
    }
    
    
    /**
    * Ecouteur sur le champ Titre valeur
    * @param Un DocumentEvent
    * @param La valeur
    */
    private void ecouteurTitreValeur(final DocumentEvent de, final Element strings_attribut, final String valeur) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteTitreV = source.getText(0, source.getLength());
            enregistrerTitreValeur(strings_attribut, valeur, texteTitreV);
            
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    
    /*************************** Operation DOM *****************************************************/
   // STRINGS_ELEMENT
    private Element enregistrerStringsElement(Element strings) {
        Element strings_element = Outils.getElementSelectionne(strings, "STRINGS_ELEMENT", ((Element)monNoeud).getAttribute("element"), "element");
        if (strings_element == null) {
            strings_element = doc.createElement("STRINGS_ELEMENT");
            strings.appendChild(strings_element);
            
            EditeurFichierConfig.setModif(true);
        }
        strings_element.setAttribute("element", ((Element)monNoeud).getAttribute("element"));
        return strings_element;
    }
    
    // TITRE
    private void enregistrerTitre(final Element parent, final String texteTitre) {
        Element titre = Outils.premierEnfantDeNom(parent, "TITRE");
        if (titre == null) {
            titre = doc.createElement("TITRE");
            parent.appendChild(titre);
        }
        Outils.setValeurElement(doc, titre, texteTitre);
    }
    
    // DOCUMENTATION
    private void enregistrerDocumentation(final Element parent, final String texteDocumentation) {
        Element documentation = Outils.premierEnfantDeNom(parent, "DOCUMENTATION");
        if (documentation == null) {
            documentation = doc.createElement("DOCUMENTATION");
            parent.appendChild(documentation);
        }
        Outils.setValeurElement(doc, documentation, texteDocumentation);
    }
    
    // STRINGS_ATTRIBUT
    private Element enregistrerStringsAttribut(final Element strings_element, final String nomAttribut) {
        Element strings_attribut = Outils.getElementSelectionne(strings_element, "STRINGS_ATTRIBUT", nomAttribut, "attribut");
        if (strings_attribut == null) {
            strings_attribut = doc.createElement("STRINGS_ATTRIBUT");
            strings_element.appendChild(strings_attribut);
            
            EditeurFichierConfig.setModif(true);
        }
        strings_attribut.setAttribute("attribut", nomAttribut);
        return strings_attribut;
    }
    
    // TITRE_VALEUR
    private Element enregistrerTitreValeur(final Element element_strings_attribut, final String valeur, final String texteTitreV) {
        Element titre_valeur = Outils.getElementSelectionne(element_strings_attribut, "TITRE_VALEUR", valeur, "valeur");
        if (titre_valeur == null) {
            titre_valeur = doc.createElement("TITRE_VALEUR");
            element_strings_attribut.appendChild(titre_valeur);
        }
        titre_valeur.setAttribute("valeur", valeur);
        Outils.setValeurElement(doc, titre_valeur, texteTitreV);
        
        return titre_valeur;
    }
    
     // nettoyer le DOM des �l�ments vide
    private void nettoyerDOM() {
        nettoyer_STRINGS_ATTRIBUT();
    }
    
    private void nettoyer_STRINGS_ATTRIBUT() {
        NodeList listeNoeud = racine.getElementsByTagName("STRINGS_ATTRIBUT");
        for (int i=0; i<listeNoeud.getLength(); i++) {
            if (listeNoeud.item(i) instanceof Element) {
                Element element_parent = (Element)listeNoeud.item(i);
                for (Node n = element_parent.getFirstChild(); n != null; n = n.getNextSibling())
                    if ("TITRE".equals(n.getNodeName()) && (!n.hasChildNodes() || "".equals(Outils.getValeurElement((Element)n))))
                        element_parent.removeChild(n);
                        
                for (Node n1 = element_parent.getFirstChild(); n1 != null; n1 = n1.getNextSibling())
                    if ("DOCUMENTATION".equals(n1.getNodeName()) && (!n1.hasChildNodes() || "".equals(Outils.getValeurElement((Element)n1))))
                        element_parent.removeChild(n1);
            }
        }
    }
}
