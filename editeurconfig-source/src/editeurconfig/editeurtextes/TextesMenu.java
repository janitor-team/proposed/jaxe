/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import javax.swing.JPanel;
import javax.swing.JButton;
import org.w3c.dom.*;
import javax.swing.text.BadLocationException;


/**
* Affiche dans un jframe l'�diteur des textes pour un menu s�lectionn�.
*/
public class TextesMenu extends JFrame {
    
    private Document doc;
    private Element racine;
    private JPanel panelAffichage;
    private Node monNoeud;
    private String menuSelectionne;
    
    private ArrayList<Node> listeNodeParents = new ArrayList<Node>();
    
    private JTextField titre_t;
    private JTextArea documentation_t;
    
    public TextesMenu(final Document doc, final Node monNoeud) {
        super(Strings.get("titre.TextePourUnMenu"));
        
        this.doc = doc;
        this.monNoeud = monNoeud;
        menuSelectionne = ((Element)monNoeud).getAttribute("nom");
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
    }
    
    
    private void afficher() {
        
        final JPanel panelFenetre = new JPanel(new BorderLayout());
        
        panelFenetre.add(new JLabel(Strings.get("label.TextePourLeMenu")+" "+menuSelectionne),BorderLayout.NORTH);
        
        panelAffichage = new JPanel();
        panelAffichage.setLayout(new BoxLayout(panelAffichage, BoxLayout.Y_AXIS));
        final JScrollPane defilement = new JScrollPane(panelAffichage);
        defilement.setPreferredSize(new Dimension(400, 700));
        panelFenetre.add(defilement,BorderLayout.CENTER);
        
        creerPanelLangues();
        
        final JPanel panelBoutons = new JPanel(new FlowLayout());
        
        final JButton bFermer = new JButton(new AbstractAction(Strings.get("bouton.Fermer")) {
            public void actionPerformed(ActionEvent e) {
                nettoyerDOM();
                setVisible(false);
            }
        });
        panelBoutons.add(bFermer);
        
        final JButton bTester = new JButton(new AbstractAction("Tester") {
            public void actionPerformed(ActionEvent e) {
                EditeurFichierConfig.convertToString(doc);
            }
        });
        //panelBoutons.add(bTester);
        
        panelFenetre.add(panelBoutons,BorderLayout.SOUTH);
        add(panelFenetre);
        
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
    
    
    /**
    * Panel de langues
    * Pour chaque langue on �dite le titre et la documentation du menu
    */
    private void creerPanelLangues() {
        
        racine = doc.getDocumentElement();
        NodeList listeNoeud = racine.getElementsByTagName("STRINGS");
        int nombreEnf = listeNoeud.getLength();
        
        final JPanel panelLangue[] = new JPanel[nombreEnf];
        
        for (int i=0; i<nombreEnf; i++) {
            if (listeNoeud.item(i) instanceof Element) {
                Element strings = (Element)listeNoeud.item(i);
                
                Node p = monNoeud;
                while (!"MENUS".equals(p.getNodeName())) {
                    listeNodeParents.add(p);
                    p = p.getParentNode();
                }
                
                final Element strings_menu = remplirStringMenu(strings);
                
                panelLangue[i] = new JPanel();
                
                panelLangue[i].setLayout(new BoxLayout(panelLangue[i], BoxLayout.Y_AXIS));
                panelAffichage.add(panelLangue[i]);
                panelLangue[i].setBorder(new TitledBorder(strings.getAttribute("langue").toUpperCase()));
                 
                final JPanel panTitre = new JPanel(new FlowLayout(FlowLayout.LEFT));
                panelLangue[i].add(panTitre);
                JLabel titre_l = new JLabel(Strings.get("label.Titre")+": ");
                titre_t = new JTextField(25);

                Element titre = null;
                if (doc != null)
                    titre = Outils.premierEnfantDeNom(strings_menu, "TITRE");
                if (titre != null)
                    titre_t.setText(Outils.getValeurElement(titre));
                
                // �couteur sur titre
                titre_t.getDocument().addDocumentListener(new DocumentListener() {
                    public void changedUpdate(DocumentEvent de) {
                        ecouteurTitre(de, strings_menu);
                    }
                    public void insertUpdate(DocumentEvent de) {
                        ecouteurTitre(de, strings_menu);
                    }
                    public void removeUpdate(DocumentEvent de) {
                        ecouteurTitre(de, strings_menu);
                    }
                });
                
                panTitre.add(titre_l);
                panTitre.add(titre_t);
                
                final JPanel panDocumentation = new JPanel(new GridLayout(2,1));
                panelLangue[i].add(panDocumentation);
                JLabel documentation_l = new JLabel(Strings.get("label.Documentation")+": ");
                documentation_t = new JTextArea(4, 12);
                documentation_t.setLineWrap(true);
                documentation_t.setWrapStyleWord(true);
                JScrollPane scrollDoc = new JScrollPane(documentation_t);
                
                Element documentation = null;
                if (doc != null)
                    documentation = Outils.premierEnfantDeNom(strings_menu, "DOCUMENTATION");
                if (documentation != null)
                    documentation_t.setText(Outils.getValeurElement(documentation));
                    
                // �couteur sur documentation
                documentation_t.getDocument().addDocumentListener(new DocumentListener() {
                    public void changedUpdate(DocumentEvent de) {
                        ecouteurDocumentation(de, strings_menu);
                    }
                    public void insertUpdate(DocumentEvent de) {
                        ecouteurDocumentation(de, strings_menu);
                    }
                    public void removeUpdate(DocumentEvent de) {
                        ecouteurDocumentation(de, strings_menu);
                    }
                });
                
                panDocumentation.add(documentation_l);
                panDocumentation.add(scrollDoc);
            }
        }
    }
    
    
    /*************************** Ecouteurs ************************************/
    /**
    * Ecouteur sur le champ Titre
    * @param Un DocumentEvent
    * @param L'�l�ment parent
    */
    private void ecouteurTitre(final DocumentEvent de, final Element strings_menu) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteTitre = source.getText(0, source.getLength());
            enregistrerTitre(strings_menu, texteTitre);
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
            System.out.println(bex);
        }
    }
    
    /**
    * Ecouteur sur le champ Documentation
    * @param Un DocumentEvent
    * @param L'�l�ment parent
    */
    private void ecouteurDocumentation(final DocumentEvent de, final Element strings_menu) {
        javax.swing.text.Document source = de.getDocument();
        try {
            String texteDocumentation = source.getText(0, source.getLength());
            enregistrerDocumentation(strings_menu, texteDocumentation);
            EditeurFichierConfig.setModif(true);
        }
        catch (BadLocationException bex) {
        }
    }
    
    
    /**
    * Remplie DOM, STRINGS_MENU
    * @param Le menu parent, l'�l�ment
    */
    private Element remplirStringMenu(final Element strings) {
        Element stringsM = strings;
        for (int j = listeNodeParents.size()-1; j >= 0; j--) {
        Element e = (Element)listeNodeParents.get(j);
            stringsM = enregistrerStringsMenu(stringsM, e.getAttribute("nom"));
        }
                
        return stringsM;
    }
    
    /*************************** Operation DOM *****************************************************/
    // STRINGS_MENU
    private Element enregistrerStringsMenu(final Element stringsM, final String titre) {
        Element strings_menu = Outils.getElementSelectionne(stringsM, "STRINGS_MENU", titre, "menu");
        if (strings_menu == null) {
            strings_menu = doc.createElement("STRINGS_MENU");
            stringsM.appendChild(strings_menu);
            strings_menu.setAttribute("menu", titre);   
            
            EditeurFichierConfig.setModif(true);
        }
        return strings_menu;
    }
    
    // TITRE
    private void enregistrerTitre(final Element strings_menu, final String texteTitre) {
        Element titre = Outils.premierEnfantDeNom(strings_menu, "TITRE");
        if (titre == null) {
            titre = doc.createElement("TITRE");
            strings_menu.appendChild(titre);
        }
        Outils.setValeurElement(doc, titre, texteTitre);
    }
    
    // DOCUMENTATION
    private void enregistrerDocumentation(final Element strings_menu, final String texteDocumentation) {
        Element documentation = Outils.premierEnfantDeNom(strings_menu, "DOCUMENTATION");
        if (documentation == null) {
            documentation = doc.createElement("DOCUMENTATION");
            strings_menu.appendChild(documentation);
        }
        Outils.setValeurElement(doc, documentation, texteDocumentation);
    }
    
    // nettoyer le DOM des �l�ments vide
    private void nettoyerDOM() {
        nettoyer_STRINGS_MENU();
    }
    
    private void nettoyer_STRINGS_MENU() {
        NodeList listeNoeud = racine.getElementsByTagName("STRINGS_MENU");
        for (int i=0; i<listeNoeud.getLength(); i++) {
            if (listeNoeud.item(i) instanceof Element) {
                Element element_parent = (Element)listeNoeud.item(i);
                for (Node n = element_parent.getFirstChild(); n != null; n = n.getNextSibling())
                    if ("TITRE".equals(n.getNodeName()) && (!n.hasChildNodes() || "".equals(Outils.getValeurElement((Element)n))))
                        element_parent.removeChild(n);
                        
                for (Node n1 = element_parent.getFirstChild(); n1 != null; n1 = n1.getNextSibling())
                    if ("DOCUMENTATION".equals(n1.getNodeName()) && (!n1.hasChildNodes() || "".equals(Outils.getValeurElement((Element)n1))))
                        element_parent.removeChild(n1);
            }
        }
    }
}
