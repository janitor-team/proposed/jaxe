/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import javax.swing.*;
import org.w3c.dom.*;
import java.io.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import javax.swing.text.MaskFormatter;
import java.text.ParseException;


/**
 * Classe permet de d�finir les langues d'un fichier de config
 */
public class DefinirLaLangue extends JDialog {
    
    private EditeurFichierConfig appl;
    private Document doc;
    private Element racine;    
    
    private JList listeLg;
    private DefaultListModel model;   
    
    private JButton bOK;
    private JButton bOKK;
    
    
    public DefinirLaLangue(final EditeurFichierConfig appl, final Document doc, final Element racine) {
        super(new JFrame(), Strings.get("titre.DefinirLesLangues"));
        this.appl = appl;
        this.doc = doc;
        this.racine = racine;
        
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        afficher();
    }
    
    
    /**
    * Affiche un dialogue ou on peut ajouter ou retirer des langues
    */
    private void afficher() {
        
        JPanel panelB = new JPanel(new FlowLayout());
        
        JLabel texteLabel = new JLabel(Strings.get("label.choixLangues"));
        add(texteLabel, BorderLayout.NORTH);
        
        model = new DefaultListModel();
        listeLg = new JList(model);
        listeLg.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        add(new JScrollPane(listeLg), BorderLayout.CENTER);
        
        final JButton bAjouter = new JButton(new AbstractAction(Strings.get("bouton.Ajouter")) {
            public void actionPerformed( ActionEvent e ) {
                ajouterLangue();
            }
        });
        panelB.add(bAjouter);
        
        final JButton bRetirer = new JButton(new AbstractAction(Strings.get("bouton.Retirer")) {
            public void actionPerformed( ActionEvent e ) {
                if (model.getSize() > 0) {                    
                    Object selectionne[] = listeLg.getSelectedValues();
                    for (int i = selectionne.length - 1; i >= 0; --i) {
                        model.removeElement(selectionne[i]);
                    }
                    //listeLg.getSelectionModel().clearSelection();
                }
                if (model.isEmpty()) bOK.setEnabled(false);
            }
        });
        panelB.add(bRetirer);
        
        final JButton bAnnuler = new JButton(new AbstractAction(Strings.get("bouton.Annuler")) {
            public void actionPerformed( ActionEvent e ) {
                setVisible(false);
                new EditeurFichierConfig();
            }
        });
        panelB.add(bAnnuler);
        
        bOK = new JButton(new AbstractAction(Strings.get("bouton.OK")) {
            public void actionPerformed( ActionEvent e ) {
                enregistrerElementStrings();
                setVisible(false);
                new ChoixDuSchema(appl, doc, racine);
            }
        });
        panelB.add(bOK);
        bOK.setEnabled(false);
        
        add(panelB, BorderLayout.SOUTH);
        //getRootPane().setDefaultButton(bOK);
        pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        setVisible(true);
    }
    
    private void ajouterLangue() {
        final JDialog jd = new JDialog(this, Strings.get("titre.SaisirLaLangue"));
        jd.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        final JPanel panel = new JPanel(new FlowLayout());
        
        final JTextField texte = new JTextField(6);
    
        panel.add(texte);
        texte.addKeyListener(new KeyListener() {
            public void keyTyped(KeyEvent e) {
                if(texte.getText().length() > 2 || texte.getText().length() < 2) {
                    texte.setForeground(Color.red);
                    bOKK.setEnabled(false);
                }
                else {
                    texte.setForeground(Color.black);
                    bOKK.setEnabled(true);
                }
            }
            public void keyPressed(KeyEvent e) {
                if(texte.getText().length() > 2 || texte.getText().length() < 2) {
                    texte.setForeground(Color.red);
                    bOKK.setEnabled(false);
                }
                else {
                    texte.setForeground(Color.black);
                    bOKK.setEnabled(true);
                }
            }
            public void keyReleased(KeyEvent e) {
                if(texte.getText().length() > 2 || texte.getText().length() < 2) {
                    texte.setForeground(Color.red);
                    bOKK.setEnabled(false);
                }
                else {
                    texte.setForeground(Color.black);
                    bOKK.setEnabled(true);
                }
            }
        });
        
        final JButton bAnnuler = new JButton(new AbstractAction(Strings.get("bouton.Annuler")) {
            public void actionPerformed( ActionEvent e ) {
                jd.setVisible(false);
            }
        });
        panel.add(bAnnuler);
        
        bOKK = new JButton(new AbstractAction(Strings.get("bouton.OK")) {
            public void actionPerformed( ActionEvent e ) {
                if (!codeLangueValide(texte.getText())) {
                    JOptionPane.showMessageDialog(jd, Strings.get("message.CodeLangueNonValide"));
                    texte.requestFocus();
                    texte.selectAll();
                }
                else {
                    if (model.contains(texte.getText())) {
                        JOptionPane.showMessageDialog(jd, Strings.get("message.LangueExiste"));
                        texte.requestFocus();
                        texte.selectAll();
                    }
                    else {
                        jd.setVisible(false);
                        model.addElement(texte.getText());
                        if (model.getSize() > 0) bOK.setEnabled(true);
                    }
                }
            }
        });
        panel.add(bOKK);
        bOKK.setEnabled(false);
        
        jd.add(panel);
        jd.pack();
        final Dimension dim = getSize();
        final Dimension ecran = getToolkit().getScreenSize();
        jd.setLocation((ecran.width - dim.width)/2, (ecran.height - dim.height)/2);
        jd.setVisible(true);
    }
    
    
    /**
    * Enregistre les �l�ments STRINGS dans le DOM
    */
    private void enregistrerElementStrings() {
        for (int i=0; i<model.size(); i++) {
            Element strings = doc.createElement("STRINGS");
            strings.setAttribute("langue", model.get(i).toString());
            racine.appendChild(strings);
        }
    }
    
    
    /**
    * V�rifie le code de la langue suivant le format d�fini par la norme ISO-639 (2 lettres minuscules) 
    */
    private boolean codeLangueValide(final String langue) {
        String[] langages = Locale.getISOLanguages();
        if (Arrays.asList(langages).contains(langue))
            return true;
        else
            return false;
    }
}

