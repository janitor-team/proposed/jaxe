/*
editeurconfig - Editeur de Fichiers de Config de Jaxe

Copyright (C) 2011 Observatoire de Paris-Meudon

Ce programme est un logiciel libre ; vous pouvez le redistribuer et/ou le modifier conform�ment aux dispositions de la Licence Publique G�n�rale GNU, telle que publi�e par la Free Software Foundation ; version 2 de la licence, ou encore (� votre choix) toute version ult�rieure.

Ce programme est distribu� dans l'espoir qu'il sera utile, mais SANS AUCUNE GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou D'ADAPTATION A UN OBJET PARTICULIER. Pour plus de d�tail, voir la Licence Publique G�n�rale GNU .

Vous devez avoir re�u un exemplaire de la Licence Publique G�n�rale GNU en m�me temps que ce programme ; si ce n'est pas le cas, �crivez � la Free Software Foundation Inc., 675 Mass Ave, Cambridge, MA 02139, Etats-Unis.
*/

package editeurconfig;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.border.*;
import java.util.*;
import org.w3c.dom.*;

/**
* Retourne un des infos sur un noeud
*/
public class InfosNoeud {
    
    private Node domNode;
    private String titre;
    
    public InfosNoeud(final Node domNode) {
        this.domNode = domNode;
        majTitre();
    }
    
    /**
    * Mis � jour le titre d'un �l�ment
    */
    public void majTitre() {
        if (domNode instanceof Element) {
            Element el = (Element)domNode;
            if ("MENUS".equals(el.getNodeName()))
                titre = Strings.get("texte.MENUS");
            else if ("MENU".equals(el.getNodeName()))
                titre = el.getAttribute("nom");
            else if ("MENU_INSERTION".equals(el.getNodeName()))
                titre = el.getAttribute("nom");
            else if ("MENU_FONCTION".equals(el.getNodeName()))
                titre = el.getAttribute("nom");
            else if ("AFFICHAGE_ELEMENT".equals(el.getNodeName()))
                titre = el.getAttribute("element");
            else if ("EXPORT".equals(el.getNodeName()))
                titre = el.getAttribute("nom");
            else if ("STRINGS".equals(el.getNodeName()))
                titre = el.getAttribute("langue");
            else if ("DESCRIPTION_CONFIG".equals(el.getNodeName()))
                titre = Strings.get("texte.descriptionConfig");
            else if ("STRINGS_MENU".equals(el.getNodeName()))
                titre = el.getAttribute("menu");
            else if ("TITRE".equals(el.getNodeName()))
                titre = Strings.get("label.Titre");
            else if ("DOCUMENTATION".equals(el.getNodeName()))
                titre = Strings.get("label.Documentation");
            else if ("STRINGS_ELEMENT".equals(el.getNodeName()))
                titre = el.getAttribute("element");
            else if ("TITRE_VALEUR".equals(el.getNodeName()))
                titre = el.getAttribute("valeur");
            else if ("STRINGS_ATTRIBUT".equals(el.getNodeName()))
                titre = el.getAttribute("attribut");
            else if ("STRINGS_EXPORT".equals(el.getNodeName()))
                titre = el.getAttribute("export");
            else 
                titre = null;
        }
        else
            titre = null;
    }
    
    
    /**
    * Renvoie le noeud
    */
    public Node getNoeud() {
        return domNode;
    }
    
    
    @Override
    public String toString() {
        return titre;
    }
    
}
